from sklearn.svm import SVC
import psycopg2
import matplotlib.pyplot as plt
import sys
import re

def connect_and_get_cursor (dbname, user, host, password):

    connect_str = ( ' dbname='   + str(dbname)
                  + ' user='     + str(user)
                  + ' host='     + str(host)
                  + ' password=' + str(password) )
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
    return cursor

def get_all_ids_and_urls (cursor):
    cursor.execute('SELECT * from contextapi_website OFFSET 0 LIMIT 25000;')
    rows = cursor.fetchall()

    ids = []
    urls = []
    for row in rows:
        ids.append (row[0])
        urls.append (row [2])

    return ids, urls

def load_sentiment_from(filename):
    file = open(filename)
    lines = file.readlines()[1:]

    x_dict = {}
    y_dict = {}
    all_labels= set()
    website_ids = []
    website_xy_dict = {}
    websites_ids_from_xy = {}
    xy = []
    labels = []
    for line in lines:
        row = line.split()
        x = float(row[0])
        y = float(row[1])
        website_id = int(row[2].replace('ID_',''))
        label = row[3]
        try:
            x_dict[label].append(x)
        except:
            x_dict[label] = [x]
        try:
            y_dict[label].append(y)
        except:
            y_dict[label] = [y]
        website_ids.append(website_id)
        all_labels.add(label)
        website_xy_dict[website_id] = (x,y)
        websites_ids_from_xy[(x,y)] = website_id
        xy.append((x,y))
        labels.append(label)

    all_labels = sorted(list(all_labels))
    labels_dict = {all_labels[i]: i for i in range(len(all_labels))}

    return x_dict, y_dict, website_xy_dict, labels_dict, all_labels, xy, labels, websites_ids_from_xy

def load_urls(filename):
    file = open(filename)
    lines = file.readlines()[1:]
    ids = []
    urls = []
    for line in lines:
        rows = line.split()
        ids.append(int(rows[0]))
        urls.append(rows[1])

    return ids, urls


def plot_sentiment_with_url (x_dict, y_dict, labels_dict, all_labels):
    colors = ['r','g','b','y','m','c','k']
    fig, ax = plt.subplots()
    for label in all_labels:
        x_axis = x_dict[label]
        y_axis = y_dict[label]
        if len(x_axis) > 130 and len(x_axis) < 10000:
            ax.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= label)
        #else:
        #    ax.scatter(x_axis, y_axis, marker='.', color='k', label= 'else')
        ax.legend(loc='best')

x_dict, y_dict, website_xy_dict, labels_dict, all_labels, xy, labels, websites_ids_from_xy = load_sentiment_from ('results/svn-groups-rbf0.05-13-no-noise.txt')

ids, urls = load_urls('website_data/urls.txt')
cleared_urls = []

for item in urls:
    url = item.replace('http://', '').replace('https://', '').split('/')[0]
    x_dict[url] = []
    y_dict[url] = []
all_labels = set()
labels_dict = {}
for i, item in enumerate(urls):
    url = item.replace('http://','').replace('https://','').split('/')[0]
    id = ids[i]
    try:
        (x,y) = website_xy_dict[id]
        if x == -1 and y == -1:
            continue
        all_labels.add(url)
        x_dict[url].append (x)
        y_dict[url].append (y)
    except:
        pass
all_labels = sorted(list(all_labels), key= lambda x: len(x_dict[x]))
labels_dict = {all_labels[i]: i for i in range(len(all_labels))}

plot_sentiment_with_url (x_dict, y_dict, labels_dict, all_labels)
plt.show()
