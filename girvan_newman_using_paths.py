import matplotlib.pyplot as plt
import networkx as nx
from scipy.cluster import hierarchy
from scipy.spatial import distance
from collections import defaultdict
import numpy
import sys

def girvan_newman (G):

    if len(G.nodes()) == 1:
        return [G.nodes()]

    def find_best_edge(G0):
        """
        Networkx implementation of edge_betweenness
        returns a dictionary. Make this into a list,
        sort it and return the edge with highest betweenness.
        """
        eb = nx.edge_betweenness_centrality(G0)
        eb_il = eb.items()
        eb_il = sorted(eb_il, key=lambda x: x[1], reverse=True)
        return eb_il[0][0]

    components = list(nx.connected_component_subgraphs(G))
    while len(components) == 1:
        G.remove_edge(*find_best_edge(G))
        components = list(nx.connected_component_subgraphs(G))

    result = [c.nodes() for c in components]

    for c in components:
        result.extend(girvan_newman(c))

    return result

def load_sentiment_from(filename):
    file = open(filename)
    lines = file.readlines()[1:]

    x_dict = {}
    y_dict = {}
    all_labels= set()
    website_ids = []
    website_xy_dict = {}
    for line in lines:
        row = line.split()
        x = float(row[0])
        y = float(row[1])
        website_id = int(row[2].replace('ID_',''))
        label = row[3]
        try:
            x_dict[label].append(x)
        except:
            x_dict[label] = [x]
        try:
            y_dict[label].append(y)
        except:
            y_dict[label] = [y]
        website_ids.append(website_id)
        all_labels.add(label)
        website_xy_dict[website_id] = (x,y)

    all_labels = sorted(list(all_labels))
    labels_dict = {all_labels[i]: i for i in range(len(all_labels))}

    return x_dict, y_dict, website_xy_dict, labels_dict, all_labels

def plot_sentiment (all_clusters):
    colors = ['r','g','b','y','m','c','k']
    for label in all_labels:

        x_axis = x_dict[label]
        y_axis = y_dict[label]
        plt.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= label)
    plt.legend(loc='best')

def plot_girvan_newman_clusters (all_clusters):
    colors = ['r','g','b','y','m','c','k']
    for label in all_labels:

        x_axis = x_dict[label]
        y_axis = y_dict[label]
        plt.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= label)
    plt.legend(loc='best')

def get_sequence_edges (website_sequence_dict, website_xy_dict, temporal_cluster_num):
    sequence_x = []
    sequence_y = []
    sequence_labels_dict = {}
    website_id_sequence = []

    index = 0
    for website_id in website_sequence_dict[temporal_cluster_num]:
        try:
            x, y = website_xy_dict[website_id]
            if x == -1 and y == -1:
                continue
            index += 1
            website_id_sequence.append(website_id)
            sequence_x.append(x)
            sequence_y.append(y)
            sequence_labels_dict [str(index)] = (x,y)
        except:
            pass
    edges = []
    for i in range(len(website_id_sequence)-1):
        curr = website_id_sequence [i]
        next = website_id_sequence [i+1]
        edges.append ([curr,next])
    if len(edges) > 0:
        edges.append([edges[-1][0],-1])
    return edges

def load_temporal_clusters (filename):
    file = open(filename)
    lines = file.readlines()[1:]
    website_sequence_dict = {}
    for line in lines:
        rows = line.replace(' ','').split(';')
        id = int(rows [0])
        websites_list = rows [2].replace('[','').replace(']','').replace('\n','').split(',')
        websites_list = [int(id) for id in websites_list]
        website_sequence_dict[id] = websites_list
    return website_sequence_dict

x_dict, y_dict, website_xy_dict, labels_dict, all_labels = load_sentiment_from ('results/svn-groups-rbf0.05-6-no-noise.txt')
website_sequence_dict = load_temporal_clusters ('results/temporal_clusters.txt')
start = 0
end = 5000
all_edges = []
for id in website_sequence_dict.keys():
    if id > start and id < end:
        edges = get_sequence_edges (website_sequence_dict, website_xy_dict, id)
        all_edges += edges


website_xy_dict[-1] = (-100,-100)
g = nx.Graph ()
g.add_nodes_from (website_xy_dict.keys ())
g.add_edges_from (all_edges)
nx.draw (g,website_xy_dict, node_shape = '.')
plt.show ()

comp = girvan_newman(g)
colors = ['r', 'g', 'b', 'y', 'm', 'c', 'k']
i = 0
for cluster in comp:
    gg = nx.Graph()
    gg.add_nodes_from (cluster)
    nx.draw(gg, website_xy_dict, node_shape = '.', node_color=colors[i%len(colors)])
    i += 1
plt.show()

