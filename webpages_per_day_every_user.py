import psycopg2
import datetime
import numpy as np
import matplotlib.pyplot as plt
import functools
from sklearn.cluster import DBSCAN
from sklearn.metrics import mean_squared_error

def connect_and_get_cursor (dbname, user, host, password):

    connect_str = ( ' dbname='   + str(dbname)
                  + ' user='     + str(user)
                  + ' host='     + str(host)
                  + ' password=' + str(password) )
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
    return cursor

def get_user_timestamps_dict_and_user_list (cursor):
    cursor.execute('SELECT * from contextapi_trackingevent')
    rows = cursor.fetchall()

    user_timestamps = {1: []}
    user_set = set()
    for row in rows:
        timestamp_vector = []
        try:
            timestamp_vector = user_timestamps[int(row[3])]
        except:
            pass
        datetime_timestamp = row [1].replace(tzinfo=None)
        timestamp_in_seconds = (datetime_timestamp - datetime.datetime(2016, 9, 1)).total_seconds()
        timestamp_vector.append (timestamp_in_seconds)
        user_timestamps [int(row[3])] = timestamp_vector
        user_set.add(row[3])

    return user_timestamps, list(user_set)



def get_pages_per_day (timestamps):
    values = np.full(len(timestamps),1.0)
    day_bins = {}

    for i in range(len(values)):
        # Integer division
        day = timestamps[i] // (60*60*24)


        if day in day_bins:
            day_bins[day] += 1
        else:
            day_bins[day] = 0

    average = 0
    day_sequence = []
    for k in sorted(list(day_bins.keys())):
        average += day_bins[k]
        day_sequence.append([k, day_bins[k]])
    average /= float(len(day_bins.keys()))

    return average, day_sequence

def find_webpages_per_day_for_specific_user (timestamp, user_id):
    clusters = get_pages_per_day(user_timestamps [user_id])
    return clusters

def plot_clusters (clusters):
    colors = ['r', 'g', 'b', 'y', 'm', 'c', 'k']
    for i, cluster in enumerate(clusters):
        x = cluster[0]
        y = cluster[1]
        plt.bar(x, y, color=colors[i%len(colors)])
    plt.show()


def plot_users (user, clusters):
    colors = ['r', 'g', 'b', 'y', 'm', 'c', 'k']
    for i, cluster in enumerate(clusters):
        x = cluster[0]
        y = cluster[1]
        plt.bar(x, y, color=colors[i%len(colors)])
    plt.title('user: ' + str(user))
    plt.show()

def get_clusters_for_all_users(user_timestamps, user_list):
    clusters = []
    user_clusters = {}
    for user_id in user_list:
        print ('Processing user ID ' + str(user_id) + '.')
        average, day_sequence = find_webpages_per_day_for_specific_user (user_timestamps, user_id)
        if average > 0:
            clusters.append([user_id, average])
            user_clusters[user_id] = day_sequence
    return clusters, user_clusters


if __name__ == '__main__':
    cursor = connect_and_get_cursor (dbname = 'cscout',
                                     user   = 'andrew',
                                     host   = 'www.csut01ltvbp1.eu-west-1.rds.amazonaws.com',
                                     password = 'efadmintestpassword')

    user_timestamps, user_list = get_user_timestamps_dict_and_user_list (cursor)
    clusters, user_clusters = get_clusters_for_all_users (user_timestamps, user_list)

    plot_clusters(clusters)
    for user in sorted(user_clusters.keys()):
        print (user_clusters[user])
        plot_users(user, user_clusters[user])