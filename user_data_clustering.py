import psycopg2
import datetime
import numpy as np
import matplotlib.pyplot as plt
import functools
from sklearn.cluster import DBSCAN
from sklearn.metrics import mean_squared_error

def connect_and_get_cursor (dbname, user, host, password):

    connect_str = ( ' dbname='   + str(dbname)
                  + ' user='     + str(user)
                  + ' host='     + str(host)
                  + ' password=' + str(password) )
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
    return cursor

def get_user_timestamps_dict_and_user_list (cursor):
    cursor.execute('SELECT * from contextapi_trackingevent')
    rows = cursor.fetchall()

    user_timestamps = {1: []}
    user_set = set()
    for row in rows:
        timestamp_vector = []
        try:
            timestamp_vector = user_timestamps[int(row[3])]
        except:
            pass
        datetime_timestamp = row [1].replace(tzinfo=None)
        timestamp_in_seconds = (datetime_timestamp - datetime.datetime(2016, 9, 1)).total_seconds()
        timestamp_vector.append (timestamp_in_seconds)
        user_timestamps [int(row[3])] = timestamp_vector
        user_set.add(row[3])

    return user_timestamps, list(user_set)

def get_clusters (timestamps, eps):
    iteration = np.zeros(len(timestamps))

    iteration_and_timestamps = []
    for index, _ in enumerate (iteration):
        iteration_and_timestamps.append([timestamps[index], iteration[index]])
    sklearn_clusters = DBSCAN (eps=eps, min_samples=1).fit(iteration_and_timestamps)

    clusters = {}
    x = []
    y = []
    for i, item in enumerate (sklearn_clusters.labels_):
        vector = []
        try:
            vector = clusters [item]
        except:
            pass
        vector.append (timestamps[i])
        x.append (item)
        y.append (timestamps[i])
        clusters [item] = vector

    cluster_vectors = []
    for key in clusters.keys():
        cluster_vectors.append (clusters [key])

    return cluster_vectors

def find_best_eps (timestamp):

    num_clusters = []
    for eps in range(1, 300):
        clusters = get_clusters(timestamp, eps=eps)
        num_clusters.append(len(clusters))

    dx = 1.0
    d_num_clusters = np.diff(num_clusters) / dx
    rms = np.sqrt(mean_squared_error(np.zeros(len(d_num_clusters)), d_num_clusters) / (len(d_num_clusters) - 1))
    threshold = rms * 5

    eps = 130  # Arbitrary default value for eps
    for rindex, ritem in enumerate(reversed(d_num_clusters)):
        if (ritem < -threshold):
            eps = len(d_num_clusters) - rindex
            break
    return eps

def find_clusters_for_specific_user (timestamp, user_id):
    eps = find_best_eps(user_timestamps [user_id])
    clusters = get_clusters(user_timestamps [user_id], eps=eps)
    return clusters

def plot_clusters (clusters):
    for index, cluster in enumerate(clusters):
        index_vector = np.full(len(cluster), index)
        plt.plot(cluster, index_vector, marker='o', lw=0)
    plt.show()

def compute_and_print_cluster_stats (clusters):
    try:
        clusters = sorted (clusters, key=functools.cmp_to_key (lambda x, y: max(x) < min(y)))

        avg_length = 0
        length_list = []
        for cluster in clusters:
            length = cluster [-1] - cluster[0]
            length_list.append(length)
            avg_length += length
        print(' Average cluster length: ' + str(avg_length / len(clusters)) + ' seconds.')
        print(' Min cluster length: ' + str(min(length_list)) + ' seconds.')
        print(' Max cluster length: ' + str(max(length_list)) + ' seconds.')

        avg_distance = 0
        distance_list = []
        for i in range(len(clusters)-1):
            distance = min(clusters [i+1]) - max(clusters[i])
            distance_list.append(distance)
            avg_distance += distance
        print (' Average separation between clusters: ' + str(avg_distance/(len(clusters)-1)) + ' seconds.')
        print (' Min separation between clusters: ' + str(min(distance_list)) + ' seconds.')
        print (' Max separation between clusters: ' + str(max(distance_list)) + ' seconds.')
    except:
        pass

def get_clusters_for_all_users(user_timestamps, user_list):
    clusters = []
    for user_id in user_list:
        print ('Processing user ID ' + str(user_id) + '.')
        user_clusters = find_clusters_for_specific_user (user_timestamps, user_id)
        compute_and_print_cluster_stats (user_clusters)
        clusters += user_clusters
    clusters = sorted(clusters, key=lambda x: min(x))
    return clusters


if __name__ == '__main__':
    cursor = connect_and_get_cursor (dbname = 'cscout',
                                     user   = 'andrew',
                                     host   = 'www.csut01ltvbp1.eu-west-1.rds.amazonaws.com',
                                     password = 'efadmintestpassword')

    user_timestamps, user_list = get_user_timestamps_dict_and_user_list (cursor)
    clusters = get_clusters_for_all_users (user_timestamps, user_list)

    plot_clusters(clusters)
    plt.savefig('all_user.jpg')
