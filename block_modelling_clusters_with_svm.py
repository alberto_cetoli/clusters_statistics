from sklearn.svm import SVC
import matplotlib.pyplot as plt
import sys

def load_sentiment_from(filename):
    file = open(filename)
    lines = file.readlines()[1:]

    x_dict = {}
    y_dict = {}
    all_labels= set()
    website_ids = []
    website_xy_dict = {}
    xy = []
    labels = []
    for line in lines:
        row = line.split()
        x = float(row[0])
        y = float(row[1])
        website_id = int(row[2].replace('ID_',''))
        label = row[3]
        try:
            x_dict[label].append(x)
        except:
            x_dict[label] = [x]
        try:
            y_dict[label].append(y)
        except:
            y_dict[label] = [y]
        website_ids.append(website_id)
        all_labels.add(label)
        website_xy_dict[website_id] = (x,y)
        xy.append((x,y))
        labels.append(label)

    all_labels = sorted(list(all_labels))
    labels_dict = {all_labels[i]: i for i in range(len(all_labels))}

    return x_dict, y_dict, website_xy_dict, labels_dict, all_labels, xy, labels, website_ids

def plot_sentiment (x_dict, y_dict, labels_dict, all_labels):
    colors = ['r','g','b','y','m','c','k']
    for label in all_labels:
        x_axis = x_dict[label]
        y_axis = y_dict[label]
        plt.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= label)
    plt.legend(loc='best')

    
x_dict, y_dict, website_xy_dict, labels_dict, all_labels, xy, labels, partial_website_ids = load_sentiment_from ('results/block-modelling-groups-13.txt')
plot_sentiment (x_dict, y_dict, labels_dict, all_labels)
plt.show()
_, _, _, _, _, x_and_y, _, website_ids = load_sentiment_from ('results/svn-groups-rbf0.05-13-no-noise.txt')

svc = SVC(decision_function_shape='ovr', kernel='rbf', gamma = 0.2)
svc.fit(xy, labels)
sklearn_labels = svc.predict(x_and_y)

print (sklearn_labels)

x_dict = {-1: []}
y_dict = {-1: []}
for i in labels:
    x_dict[i] = []
    y_dict[i] = []

all_labels = set()
semantic_ids = []
for i, label in enumerate(sklearn_labels):
    x_dict[label].append(x_and_y[i][0])
    y_dict[label].append(x_and_y[i][1])
    all_labels.add(label)
    semantic_ids.append (label)

file = open ('results/svn-blockmodelling-groups.txt', 'w')
file.write ('There are ' + str(len(all_labels)) + ' labels.\n')
for i, label in enumerate(sklearn_labels):
    file.write (str(x_and_y[i][0]) + ' '
                + str(x_and_y[i][1]) + ' '
                + str(website_ids[i]) + ' '
                + str(label) + '\n')

colors = ['m','y','b','c','g','r','k']
i = 0
for label in all_labels:
    i += 1
    x_axis = x_dict[label]
    y_axis = y_dict[label]
    if len(x_axis) < 20:
        continue
    plt.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= label)

plt.legend(loc='best')
plt.show()



