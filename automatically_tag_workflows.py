import matplotlib.pyplot as plt
import numpy as np
import sys
from gensim.models import Doc2Vec

def load_sentiment_from(filename):
    file = open(filename)
    lines = file.readlines()[1:]
    tagging_per_id_dict = {}

    x_dict = {}
    y_dict = {}
    all_labels= set()
    website_ids = []
    website_xy_dict = {}
    for line in lines:
        row = line.split()
        x = float(row[0])
        y = float(row[1])
        website_id = int(row[2].replace('ID_',''))
        label = row[3]
        try:
            x_dict[label].append(x)
        except:
            x_dict[label] = [x]
        try:
            y_dict[label].append(y)
        except:
            y_dict[label] = [y]
        website_ids.append(website_id)
        all_labels.add(label)
        website_xy_dict[website_id] = (x,y)
        tagging_per_id_dict[website_id] = label

    all_labels = sorted(list(all_labels))
    labels_dict = {all_labels[i]: i for i in range(len(all_labels))}

    return x_dict, y_dict, website_xy_dict, labels_dict, all_labels, tagging_per_id_dict

def find_closest_word2vec_tags_of_one_cluster (website_sequence_dict, model, tagging_per_id_dict, website_xy_dict, temporal_cluster_num):
    sequence = []

    index = 0
    for website_id in website_sequence_dict[temporal_cluster_num]:
        try:
            if website_xy_dict[website_id] == (-1,-1): # only here for consistency with the path created in sentiment_path_in_temporal_clusters.py
                continue
            x = model.docvecs['ID_'+str(website_id)]
            sequence.append(x)
        except:
            pass
    id = ''
    if len(sequence) > 0:
        id_str = model.docvecs.most_similar ( sequence ) [0][0]
        id = int(id_str.replace('ID_',''))
    try:
        return tagging_per_id_dict[id]
    except:
        return 'none'

def load_temporal_clusters (filename):
    file = open(filename)
    lines = file.readlines()[1:]
    website_sequence_dict = {}
    for line in lines:
        rows = line.replace(' ','').split(';')
        id = int(rows [0])
        websites_list = rows [2].replace('[','').replace(']','').replace('\n','').split(',')
        websites_list = [int(id) for id in websites_list]
        website_sequence_dict[id] = websites_list
    return website_sequence_dict

def find_all_tags (website_sequence_dict, model, tagging_per_id_dict, website_xy_dict, start, end):
    all_tags = []
    all_ids = []
    for id in website_sequence_dict.keys():
        if id >= start and id < end:
            tag = find_closest_word2vec_tags_of_one_cluster(website_sequence_dict, model, tagging_per_id_dict, website_xy_dict, id)
            all_ids.append(id)
            all_tags.append(tag)
    return all_tags, all_ids


def plot_lengths (bin_dict, dx):
    x_list = []
    y_list = []
    for length in bin_dict.keys():
        x_list.append(length)
        y_list.append(bin_dict[length])
    plt.bar(x_list, y_list, width = dx)

x_dict, y_dict, website_xy_dict, labels_dict, all_labels, tagging_per_id_dict = load_sentiment_from ('results/svn-groups-rbf0.05-7-no-noise.txt')
website_sequence_dict = load_temporal_clusters ('results/temporal_clusters.txt')


model = Doc2Vec.load('./nltk-websites-500.d2v')
start = 0
end = 5000
all_tags, all_ids = find_all_tags (website_sequence_dict, model, tagging_per_id_dict, website_xy_dict, start, end)
for i, tag in enumerate (all_tags):
    if tag != 'none':
        print (str(all_ids[i]) + '; ' + str(website_sequence_dict[all_ids[i]]) + '; ' + tag)