from gensim import utils
from gensim.models import Doc2Vec
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

model = Doc2Vec.load('./websites-475.d2v')

doc_vectors = []
doc_ids = list(model.docvecs.doctags.keys())
for id in doc_ids:
    doc_vectors.append (np.array(model.docvecs[id]))


coordinates = []
for index, vector in enumerate(doc_vectors):
    coordinates.append([vector[i] for i in range(len(vector))])

eps = 3.5
sklearn_clusters = DBSCAN(eps=eps, min_samples=5).fit(coordinates)
all_labels = set ()
for _, label in enumerate(sklearn_clusters.labels_):
    all_labels.add(label)
print (eps, len(all_labels))


file = open ('results/100dim_clustering.txt', 'w')
for i, label in enumerate(sklearn_clusters.labels_):
    file.write (str([i, doc_ids[i], label])+'\n')