from gensim import utils
from gensim.models import Doc2Vec
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn.svm import SVC
from sklearn.cluster import DBSCAN
from sklearn.cluster import MeanShift, estimate_bandwidth
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from bhtsne import tsne
import psycopg2


ZERO = 0
def get_vector(i,vocab_size):
    vect = [ZERO]*vocab_size
    vect[i] = 1.
    return vect


def connect_and_get_cursor (dbname, user, host, password):

    connect_str = ( ' dbname='   + str(dbname)
                  + ' user='     + str(user)
                  + ' host='     + str(host)
                  + ' password=' + str(password) )
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
    return cursor

def get_all_session_ids_and_labels (cursor):
    cursor.execute('SELECT * from contextapi_sessionlabel;')
    rows = cursor.fetchall()

    session_to_labels = {}
    labels = set()
    for row in rows:
        tag = row[1].lower().strip()
        session_to_labels [int(row[0])] = tag
        labels.add(tag)
    labels = list(labels)
    labels = sorted(labels)

    return session_to_labels, labels

def get_tracking_session_id_dict (cursor):
    cursor.execute('SELECT * from contextapi_sessionevent;')
    rows = cursor.fetchall ()

    tracking_to_session = {}
    for row in rows:
        tracking_to_session[row[2]] = row[1]

    return tracking_to_session


def get_tracking_website_id_dict (cursor):
    cursor.execute('SELECT * from contextapi_trackingevent;')
    rows = cursor.fetchall ()

    tracking_to_website = {}
    for row in rows:
        tracking_to_website[row[0]] = row[2]

    return tracking_to_website

def load_centroid_vectors_and_get_id_dict ():
    file = open('results/centroid-coordinates-docs.txt')
    id_to_centroid = {}
    for line in file.readlines():
        items = line.split('|')
        id = int(items[0].replace('ID_',''))
        centroid_coord = [float(item) for item in items[1].replace('\n', '').replace('[', '').replace(']', '').split(',')]
        centroid_coord = centroid_coord / np.linalg.norm(centroid_coord)
        id_to_centroid[id] = centroid_coord
    return id_to_centroid



model = Doc2Vec.load('./websites-475.d2v')
print (model.most_similar('key'))
test_vect = model.infer_vector(['The','commission','launched','an','investigation'])
print (test_vect)
print (model.docvecs['ID_1'])
print (model.docvecs.most_similar(['ID_4793']))
print (len(model.docvecs))

doc_vectors = []
doc_ids = list(model.docvecs.doctags.keys())
for id in doc_ids:
    doc_vectors.append (np.array(model.docvecs[id], dtype=np.float64))

MAX_VECTORS= len(doc_vectors)
#MAX_VECTORS = 5000

doc_vectors = np.array(doc_vectors[:MAX_VECTORS])
doc_ids = doc_ids[:MAX_VECTORS]
    

# uses PCA to find the biggest cluster (the part without "noise")
pca = PCA (n_components=100)
projected = pca.fit_transform (doc_vectors)
print (pca.explained_variance_ratio_)
plt.plot(pca.explained_variance_ratio_)
plt.show()

x = np.array(projected[:,0])
y = np.array(projected[:,1])

x_and_y = []
for index, _ in enumerate(x):
    x_and_y.append([x[index], y[index]])

sklearn_clusters = DBSCAN(eps=0.25, min_samples=5).fit(x_and_y)

all_labels = set()
coord_dict = {-1:[]}
for label in range(50):
    coord_dict[label] = []
    

for i, label in enumerate(sklearn_clusters.labels_):
    coord_dict[label].append(i)
    all_labels.add(label)

all_labels = list(all_labels)
all_labels = sorted(all_labels, key=lambda x: -len(coord_dict[x]))
first_label = all_labels[0]
semantic_doc_vectors = []
semantic_doc_ids = []
for index in coord_dict[first_label]:
    semantic_doc_vectors.append(doc_vectors[index])
    semantic_doc_ids.append(doc_ids[index])

semantic_doc_vectors = np.array(semantic_doc_vectors)
projected = tsne (semantic_doc_vectors, dimensions=2, perplexity=25)

x = np.array(projected[:,0])
y = np.array(projected[:,1])
fig, ax = plt.subplots()
ax.scatter(x, y, marker = '.')
#for i, id in enumerate(semantic_doc_ids):
#    ax.annotate(id, (x[i],y[i]))
plt.show()

cursor = connect_and_get_cursor(dbname='cscout',
                                user='andrew',
                                host='46.101.78.187',
                                password='efadmintestpassword')

session_to_label, labels = get_all_session_ids_and_labels (cursor)
tracking_to_session = get_tracking_session_id_dict (cursor)
tracking_to_website = get_tracking_website_id_dict (cursor)

id_to_centroid = load_centroid_vectors_and_get_id_dict ()

id_to_label = {}

for tracking_id in tracking_to_session.keys():
    session_id = tracking_to_session [tracking_id]
    try:
        id_to_centroid[tracking_to_website[tracking_id]]
        id_to_label[tracking_to_website[tracking_id]] = session_to_label [session_id]
    except:
        pass
labels_dict = {labels[i]: i for i in range(len(labels))}

labeled_x = []
labeled_y = []
labeled_x_y = []
labeled_label = []

x_dict = {}
y_dict = {}
for i in labels:
    x_dict[i] = []
    y_dict[i] = []
for i, id in enumerate(semantic_doc_ids):
    try:
        label = id_to_label[int(id.replace('ID_',''))]
        labeled_label.append(label)
        labeled_x.append(x[i])
        labeled_y.append(y[i])
        labeled_x_y.append([x[i],y[i]])
        x_dict[label].append(x[i])
        y_dict[label].append(y[i])
    except:
        pass

fig, ax = plt.subplots()
ax.scatter(labeled_x, labeled_y, marker = '.')
plt.show()

colors = ['r','g','b','y','m','c','k']
for label in labels:
    try:
        x_axis = x_dict[label]
        y_axis = y_dict[label]
        plt.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)])
    except:
        pass
plt.show()

x_and_y = []
for index, _ in enumerate(x):
    x_and_y.append([x[index], y[index]])

NUM_CLUSTERS = 1000
    
x_dict = {-1:[]}
y_dict = {-1:[]}
for i in labels:
    x_dict[i] = []
    y_dict[i] = []

x_and_y = np.array(x_and_y)
svc = SVC(decision_function_shape='ovo', kernel='rbf', gamma = 0.05)
svc.fit(labeled_x_y, labeled_label)
sklearn_labels = svc.predict(x_and_y)

all_labels = set()
for i, label in enumerate(sklearn_labels):
    x_dict[label].append(x[i])
    y_dict[label].append(y[i])
    all_labels.add(label)

file = open ('results/svn-groups.txt', 'w')
file.write ('There are ' + str(len(all_labels)) + ' labels.\n')
for i, label in enumerate(sklearn_labels):
    file.write (str(x[i]) + ' '
                + str(y[i]) + ' '
                + str(semantic_doc_ids[i]) + ' '
                + str(label) + '\n')


print (labels)
colors = ['r','g','b','y','m','c','k']
for label in all_labels:
    x_axis = x_dict[label]
    y_axis = y_dict[label]
    plt.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= label)

plt.legend(loc='best')
plt.show()


