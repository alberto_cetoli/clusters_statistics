import matplotlib.pyplot as plt
import networkx as nx
from scipy.cluster import hierarchy
from scipy.spatial import distance
from collections import defaultdict
import numpy as np
import sys
import copy
import random
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans
from gensim.models import Doc2Vec
from bhtsne import tsne

sys.setrecursionlimit(5000)


def cluster(distances, k=3):
    m = distances.shape[0]  # number of points

    # Pick k random medoids.
    curr_medoids = np.array([-1] * k)
    while not len(np.unique(curr_medoids)) == k:
        curr_medoids = np.array([random.randint(0, m - 1) for _ in range(k)])
    old_medoids = np.array([-1] * k)  # Doesn't matter what we initialize these to.
    new_medoids = np.array([-1] * k)

    # Until the medoids stop updating, do the following:
    while not ((old_medoids == curr_medoids).all()):
        # Assign each point to cluster with closest medoid.
        clusters = assign_points_to_clusters(curr_medoids, distances)

        # Update cluster medoids to be lowest cost point.
        for curr_medoid in curr_medoids:
            cluster = np.where(clusters == curr_medoid)[0]
            new_medoids[curr_medoids == curr_medoid] = compute_new_medoid(cluster, distances)

        old_medoids[:] = curr_medoids[:]
        curr_medoids[:] = new_medoids[:]

    return clusters, curr_medoids


def assign_points_to_clusters(medoids, distances):
    distances_to_medoids = distances[:, medoids]
    clusters = medoids[np.argmin(distances_to_medoids, axis=1)]
    clusters[medoids] = medoids
    return clusters


def compute_new_medoid(cluster, distances):
    mask = np.ones(distances.shape)
    mask[np.ix_(cluster, cluster)] = 0.
    cluster_distances = np.ma.masked_array(data=distances, mask=mask, fill_value=10e9)
    costs = cluster_distances.sum(axis=1)
    return costs.argmin(axis=0, fill_value=10e9)

def chunks(l, n):
    n = max(1, n)
    return [l[i:i+n] for i in range(0, len(l), n)]

def load_sentiment_from(filename):
    file = open(filename)
    lines = file.readlines()[1:]

    x_dict = {}
    y_dict = {}
    all_labels= set()
    website_ids = []
    website_xy_dict = {}
    website_id_to_xy_dict = {}
    labels_list = []
    for line in lines:
        row = line.split()
        x = float(row[0])
        y = float(row[1])
        website_id = int(row[2].replace('ID_',''))
        label = row[3]
        try:
            x_dict[label].append(x)
        except:
            x_dict[label] = [x]
        try:
            y_dict[label].append(y)
        except:
            y_dict[label] = [y]
        website_ids.append(website_id)
        all_labels.add(label)
        website_xy_dict[website_id] = (x,y)
        website_id_to_xy_dict[(x, y)] = website_id
        labels_list.append(label)

    all_labels = sorted(list(all_labels))
    labels_dict = {all_labels[i]: i for i in range(len(all_labels))}

    return x_dict, y_dict, website_xy_dict, labels_dict, all_labels, website_id_to_xy_dict, website_ids, labels_list

def plot_sentiment (all_clusters):
    colors = ['r','g','b','y','m','c','k']
    for label in all_labels:

        x_axis = x_dict[label]
        y_axis = y_dict[label]
        plt.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= label)
    plt.legend(loc='best')

def plot_girvan_newman_clusters (all_clusters):
    colors = ['r','g','b','y','m','c','k']
    for label in all_labels:

        x_axis = x_dict[label]
        y_axis = y_dict[label]
        plt.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= label)
    plt.legend(loc='best')

def get_sequence_edges (website_sequence_dict, website_xy_dict, temporal_cluster_num):
    sequence_x = []
    sequence_y = []
    sequence_labels_dict = {}
    website_id_sequence = []

    index = 0
    for website_id in website_sequence_dict[temporal_cluster_num]:
        try:
            x, y = website_xy_dict[website_id]
            if x == -1 and y == -1:
                continue
            index += 1
            website_id_sequence.append(website_id)
            sequence_x.append(x)
            sequence_y.append(y)
            sequence_labels_dict [str(index)] = (x,y)
        except:
            pass
    edges = []
    for i in range(len(website_id_sequence)-1):
        curr = website_id_sequence [i]
        next = website_id_sequence [i+1]
        edges.append ([curr,next])
    return edges

def load_temporal_clusters (filename):
    file = open(filename)
    lines = file.readlines()[1:]
    website_sequence_dict = {}
    for line in lines:
        rows = line.replace(' ','').split(';')
        id = int(rows [0])
        websites_list = rows [2].replace('[','').replace(']','').replace('\n','').split(',')
        websites_list = [int(id) for id in websites_list]
        website_sequence_dict[id] = websites_list
    return website_sequence_dict

x_dict, y_dict, website_xy_dict, labels_dict, all_labels, website_id_to_xy_dict, website_ids, labels_list = load_sentiment_from ('results/svn-groups-rbf0.05-7-no-noise.txt')
website_sequence_dict = load_temporal_clusters ('results/temporal_clusters.txt')
start = 0
end = 5000
all_edges = []
for id in website_sequence_dict.keys():
    if int(id) > start and int(id) < end:
        edges = get_sequence_edges (website_sequence_dict, website_xy_dict, id)
        all_edges += edges


g = nx.Graph ()
g.add_nodes_from (website_xy_dict.keys ())
g.add_edges_from (all_edges)


xy_list = list(website_id_to_xy_dict.keys())
#sklearn_clusters = DBSCAN(eps=0.5, min_samples=2).fit(xy_list)
sklearn_clusters = KMeans(n_clusters=1000).fit(xy_list)
xy_list_dict = {}
for i, label in enumerate(sklearn_clusters.labels_):
    try:
        xy_list_dict[label].append(website_id_to_xy_dict[xy_list[i]])
    except:
        xy_list_dict[label] = [website_id_to_xy_dict[xy_list[i]]]


nodes_chunks = []
for key in xy_list_dict.keys():
    nodes_chunks.append(xy_list_dict[key])

new_website_xy_dict = {}
cluster_dict = {}
for i, chunk in enumerate(nodes_chunks):
    coordinates = [website_xy_dict [id] for id in chunk]
    com = np.sum(coordinates,0)/len(coordinates)
    new_website_xy_dict [i] = com
    cluster_dict [i] = chunk

blocks_graph = nx.blockmodel (g, nodes_chunks, multigraph = True)
g = blocks_graph

nx.draw (g,new_website_xy_dict, node_shape = '.', alpha=0.1)
plt.show ()


distance_matrix = np.zeros((len(g.nodes()),len(g.nodes())), dtype=np.float32)
distance_matrix.fill(1.1)
for v1 in g.nodes():
    for v2 in g.nodes():
        number_of_edges = g.number_of_edges(v1,v2)
        if number_of_edges != 0:
            distance_matrix[v1][v2] = 1.0/number_of_edges

model = Doc2Vec.load('./nltk-websites-500.d2v')

clusters, curr_medoids = cluster(distance_matrix, 50)
print (len(clusters))
print (clusters)

comp = {}
for i, item in enumerate(clusters):
    try:
        comp[item].extend(cluster_dict[i])
    except:
        comp[item] = cluster_dict[i]

colors = ['r', 'g', 'b', 'y', 'm', 'c', 'k']
i = 0
for cluster_key in comp.keys():
    cluster = comp[cluster_key]
    gg = nx.Graph()
    gg.add_nodes_from (cluster)
    nx.draw(gg, website_xy_dict, node_shape = '.', node_color=colors[i%len(colors)])
    i += 1
plt.show()
