from gensim import utils
from gensim.models import Doc2Vec
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
from sklearn.cluster import MeanShift, estimate_bandwidth
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from bhtsne import tsne

model = Doc2Vec.load('./nltk-websites-500.d2v')
print (model.most_similar('key'))
test_vect = model.infer_vector(['The','commission','launched','an','investigation'])
print (test_vect)
print (model.docvecs['ID_1'])
print (model.docvecs.most_similar(['ID_4793']))
print (len(model.docvecs))

doc_vectors = []
doc_ids = list(model.docvecs.doctags.keys())
for id in doc_ids:
    doc_vectors.append (np.array(model.docvecs[id], dtype=np.float64))

MAX_VECTORS= len(doc_vectors)
    
doc_vectors = np.array(doc_vectors[:MAX_VECTORS])
doc_ids = doc_ids[:MAX_VECTORS]
    

# uses PCA to find the biggest cluster (the part without "noise")
pca = PCA (n_components=100)
projected = pca.fit_transform (doc_vectors)
print (pca.explained_variance_ratio_)
plt.plot(pca.explained_variance_ratio_)
plt.show()

x = np.array(projected[:,0])
y = np.array(projected[:,1])

x_and_y = []
for index, _ in enumerate(x):
    x_and_y.append([y[index], x[index]])

sklearn_clusters = DBSCAN(eps=0.25, min_samples=5).fit(x_and_y)

all_labels = set()
coord_dict = {-1:[]}
for label in range(50):
    coord_dict[label] = []
    

for i, label in enumerate(sklearn_clusters.labels_):
    coord_dict[label].append(i)
    all_labels.add(label)

all_labels = list(all_labels)
all_labels = sorted(all_labels, key=lambda x: -len(coord_dict[x]))
first_label = all_labels[0]
semantic_doc_vectors = []
semantic_doc_ids = []
for index in coord_dict[first_label]:
    semantic_doc_vectors.append(doc_vectors[index])
    semantic_doc_ids.append(doc_ids[index])

semantic_doc_vectors = np.array(semantic_doc_vectors)
projected = tsne (semantic_doc_vectors, dimensions=2, perplexity=25)

x = np.array(projected[:,0])
y = np.array(projected[:,1])
fig, ax = plt.subplots()
ax.scatter(x, y, marker = '.')
#for i, id in enumerate(semantic_doc_ids):
#    ax.annotate(id, (x[i],y[i]))
plt.show()

x_and_y = []
for index, _ in enumerate(x):
    x_and_y.append([y[index], x[index]])

NUM_CLUSTERS = 1000
    
x_dict = {-1:[]}
y_dict = {-1:[]}
for i in range(NUM_CLUSTERS):
    x_dict[i] = []
    y_dict[i] = []

x_and_y = np.array(x_and_y)
#sklearn_clusters = DBSCAN(eps=1.25, min_samples=10).fit(x_and_y)
sklearn_clusters = MeanShift(bandwidth=estimate_bandwidth(x_and_y, quantile=0.02), bin_seeding=False).fit(x_and_y)
all_labels = set()
for i, label in enumerate(sklearn_clusters.labels_):
    x_dict[label].append(x[i])
    y_dict[label].append(y[i])
    all_labels.add(label)

file = open ('results/tsne-groups.txt', 'w')
file.write ('There are ' + str(len(all_labels)) + ' labels.\n')
for i, label in enumerate(sklearn_clusters.labels_):
    file.write (str(x[i]) + ' '
                + str(y[i]) + ' '
                + str(semantic_doc_ids[i]) + ' '
                + str(label) + '\n')

    
colors = ['r','g','b','y','m','c','k']
for label in all_labels:
    x_axis = x_dict[label]
    y_axis = y_dict[label]
    plt.scatter(x_axis, y_axis, marker = '.', color=colors[label%len(colors)])
plt.show()


