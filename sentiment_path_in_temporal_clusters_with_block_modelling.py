import matplotlib.pyplot as plt
import sys
import nltk
import numpy as np
from gensim.models import Doc2Vec
from gensim.summarization import keywords, summarize
from gensim import utils
from os import listdir
from os.path import isfile, join

forbidden_words = []
for line in open('forbidden.txt').readlines():
    forbidden_words.append(line.replace('\n', '').strip())

forbidden_lines = []
for line in open('forbidden_lines.txt').readlines():
    forbidden_lines.append(line.replace('\n', '').strip())


def clean_text (text):
    old_text = ''
    while old_text != text:
        old_text = text
        text = text.replace('  ', ' ')
        text = text.replace('\t', ' ')
        text = text.replace('  .', '.')
        text = text.replace('  ,', ',')
        text = text.replace('  ;', ';')
        text = text.replace('  :', ':')
        text = text.replace('  ?', '?')
        text = text.replace('  !', '!')
        text = text.replace('  -', '-')
        text = text.replace('  ·', '·')
        text = text.replace('   "', ' "')
    return text

def load_sentiment_from(filename):
    file = open(filename)
    lines = file.readlines()[1:]

    x_dict = {}
    y_dict = {}
    all_labels= set()
    website_ids = []
    website_xy_dict = {}
    for line in lines:
        row = line.split()
        x = float(row[0])
        y = float(row[1])
        website_id = int(row[2].replace('ID_',''))
        label = ''
        for item in row[3:]:
            label += item
            label += ' '
        try:
            x_dict[label].append(x)
        except:
            x_dict[label] = [x]
        try:
            y_dict[label].append(y)
        except:
            y_dict[label] = [y]
        website_ids.append(website_id)
        all_labels.add(label)
        website_xy_dict[website_id] = (x,y)

    all_labels = sorted(list(all_labels))
    labels_dict = {all_labels[i]: i for i in range(len(all_labels))}

    return x_dict, y_dict, website_xy_dict, labels_dict, all_labels

def plot_sentiment (x_dict, y_dict, labels_dict, all_labels):
    colors = ['r','g','b','y','m','c','k']
    for label in all_labels:

        x_axis = x_dict[label]
        y_axis = y_dict[label]
        plt.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= label)
    plt.legend(loc='best')

def plot_temporal_cluster (website_sequence_dict, website_xy_dict, temporal_cluster_num):
    sequence_x = []
    sequence_y = []
    sequence_labels_dict = {}
    website_id_sequence = []

    index = 0
    for website_id in website_sequence_dict[temporal_cluster_num]:
        try:
            x, y = website_xy_dict[website_id]
            if x == -1 and y == -1:
                continue
            index += 1
            website_id_sequence.append(website_id)
            sequence_x.append(x)
            sequence_y.append(y)
            sequence_labels_dict [str(index)] = (x,y)
        except:
            print (website_id)
            pass
    print (sequence_x)
    for i, id in enumerate(website_id_sequence):
        print (i+1, '-', id,)
    print ('')
    for key in sequence_labels_dict.keys():
        plt.annotate(key, xy= sequence_labels_dict[key], fontsize=30)
    plt.plot(sequence_x, sequence_y, marker = '.', color='k', )

def load_temporal_clusters (filename):
    file = open(filename)
    lines = file.readlines()[1:]
    website_sequence_dict = {}
    labels_for_workflow_dict = {}
    for line in lines:
        rows = line.split(';')
        id = int(rows [0].replace(' ',''))
        websites_list = rows [1].replace(' ','').replace('[','').replace(']','').replace('\n','').split(',')
        websites_list = [int(id) for id in websites_list]
        website_sequence_dict[id] = websites_list
        labels_for_workflow_dict[id] = rows[2].strip()
    return website_sequence_dict, labels_for_workflow_dict

def infer_vector_from_doc (model, text):
    tokenizer = nltk.tokenize.TweetTokenizer()
    words = tokenizer.tokenize(utils.to_unicode(text))
    vector = model.infer_vector (words)
    return vector

def get_description (workflow_num, docs, labels_for_workflow_dict, website_sequence_dict, model):
    seq_id = int(sys.argv[1])
    label = labels_for_workflow_dict[int(sys.argv[1])]
    website_id_sequence = website_sequence_dict[seq_id]
    additional_label = get_tagged_labels(model, docs, website_id_sequence)
    label = label + ' / ' + additional_label
    summary = clean_text(get_summary(model, docs, website_id_sequence))
    return label, summary

def get_tagged_labels (model, docs, sequence):
    doc_vectors = []

    doc_keywords = []
    doc_key_vectors = []

    for item in sequence:
        text = docs [str(item)]
        doc_vector = model.docvecs['ID_'+str(item)]
        doc_vectors.append(doc_vector)

        lines = []
        text = text[0:2000]

        key_vectors = []
        keyws = keywords(text, split=True)
        for key in keyws:
            line = key
            if line == '\n': continue
            line = line.replace('\n', '').replace('  ', ' ')
            lines.append(line)

            key_vector = infer_vector_from_doc(model, line)
            key_vectors.append(key_vector)

        doc_keywords.append(keyws)
        doc_key_vectors.append(key_vectors)

    all_keywords = []
    for keys in doc_keywords:
        all_keywords.extend(keys)
    all_key_vectors = []
    for vects in doc_key_vectors:
        all_key_vectors.extend(vects)

    lines = []
    distances = []
    vectors = []
    for i, _ in enumerate(all_keywords):
        line = all_keywords[i]
        vector = all_key_vectors[i]
        lines.append(line)
        vectors.append(vector)
        distance = 0
        for j in range(len(sequence)):
            doc_vector = doc_vectors[j]
            distance += np.linalg.norm(doc_vector - vector)
        distances.append(distance)

    lvd = zip(lines, vectors, distances)
    lvd = sorted(lvd, key=lambda x: x[2])
    label = lvd[0][0]

    for i in range (len(lvd)):
        label = lvd[i][0]
        try:
            forbidden_words.index(label)
        except:
            return label

    return ''

def get_summary (model, docs, sequence):

    doc_vectors = []
    doc_lines = []
    doc_line_vectors = []

    for item in sequence:
        text = docs[str(item)]
        doc_vector = model.docvecs['ID_'+str(item)]
        doc_vectors.append(doc_vector)

        text = text[0:2000]
        summary_lines = []
        try:
            summary_lines = summarize(text.replace('  ',' ').replace('  ',' '), word_count=100, split=True)
        except:
            pass
        lines = []
        vectors = []
        for line in summary_lines:
            if line == '\n': continue
            line = line.replace('\n', '').replace('  ',' ')
            lines.append(line)

            vector = infer_vector_from_doc(model, line)
            vectors.append(vector)
        doc_lines.append(lines)
        doc_line_vectors.append(vectors)

    most_important_lines = []
    for i, _ in enumerate(sequence):
        lines = doc_lines[i]
        try:
            most_important_line = lines[0]
            for forbidden_line in forbidden_lines:
                if most_important_line.find(forbidden_line) != -1:
                    raise RuntimeWarning
            most_important_lines.append(most_important_line)
        except:
            pass

    already_printed=[]
    summary = ''
    for line in most_important_lines:
        try:
            already_printed.index(line)
        except:
            summary += line + ' '
            already_printed.append(line)
    return summary


path = 'website_data/'
all_files = [f for f in listdir(path) if isfile(join(path, f))]
WINDOW_SIZE = 20

docs = {}
for source in all_files:
    text_file = open(path+source, "r")
    text = text_file.read().replace('  ',' ').replace('  ',' ')
    id = source.split('.')[0]
    docs[id] = text

model = Doc2Vec.load('./cleaned-nltk-websites-500.d2v')

x_dict, y_dict, website_xy_dict, labels_dict, all_labels = load_sentiment_from ('results/svn-blockmodelling-groups-13.txt')
plot_sentiment (x_dict, y_dict, labels_dict, all_labels)
website_sequence_dict, labels_for_workflow_dict = load_temporal_clusters ('results/automatic_tagging_with_blockmodelling.txt')
plot_temporal_cluster (website_sequence_dict, website_xy_dict, int(sys.argv[1]))

title, summary = get_description (int(sys.argv[1]), docs, labels_for_workflow_dict, website_sequence_dict, model)

print ('---')
print (title)
print (summary)

plt.show()




