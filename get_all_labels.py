import psycopg2
import tensorflow as tf
import sys
import numpy as np
import random

ZERO = 0
def get_vector(i,vocab_size):
    vect = [ZERO]*vocab_size
    vect[i] = 1.
    return vect


def connect_and_get_cursor (dbname, user, host, password):

    connect_str = ( ' dbname='   + str(dbname)
                  + ' user='     + str(user)
                  + ' host='     + str(host)
                  + ' password=' + str(password) )
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
    return cursor

def get_all_session_ids_and_labels (cursor):
    cursor.execute('SELECT * from contextapi_sessionlabel;')
    rows = cursor.fetchall()

    session_to_labels = {}
    labels = set()
    for row in rows:
        tag = row[1].lower()
        session_to_labels [int(row[0])] = tag
        labels.add(tag)
    labels = list(labels)
    labels = sorted(labels)

    return session_to_labels, labels

def get_tracking_session_id_dict (cursor):
    cursor.execute('SELECT * from contextapi_sessionevent;')
    rows = cursor.fetchall ()

    tracking_to_session = {}
    for row in rows:
        tracking_to_session[row[2]] = row[1]

    return tracking_to_session


def get_tracking_website_id_dict (cursor):
    cursor.execute('SELECT * from contextapi_trackingevent;')
    rows = cursor.fetchall ()

    tracking_to_website = {}
    for row in rows:
        tracking_to_website[row[0]] = row[2]

    return tracking_to_website

def load_centroid_vectors_and_get_id_dict ():
    file = open('results/centroid-coordinates-docs.txt')
    id_to_centroid = {}
    for line in file.readlines():
        items = line.split('|')
        id = int(items[0].replace('ID_',''))
        centroid_coord = [float(item) for item in items[1].replace('\n', '').replace('[', '').replace(']', '').split(',')]
        centroid_coord = centroid_coord / np.linalg.norm(centroid_coord)
        id_to_centroid[id] = centroid_coord
    return id_to_centroid

if __name__ == '__main__':
    cursor = connect_and_get_cursor (dbname = 'cscout',
                                     user   = 'andrew',
                                     host   = '46.101.78.187',
                                     password = 'efadmintestpassword')

    session_to_label, labels = get_all_session_ids_and_labels (cursor)
    tracking_to_session = get_tracking_session_id_dict (cursor)
    tracking_to_website = get_tracking_website_id_dict (cursor)

    id_to_centroid = load_centroid_vectors_and_get_id_dict ()

    id_to_label = {}

    for tracking_id in tracking_to_session.keys():
        session_id = tracking_to_session [tracking_id]
        try:
            #print (tracking_id, session_id, session_to_label [session_id])
            #print (tracking_to_website[tracking_id])
            id_to_centroid[tracking_to_website[tracking_id]]
            id_to_label[tracking_to_website[tracking_id]] = session_to_label [session_id]
        except:
            pass

    vect_size = 10
    tag_size = len(labels)

    config = tf.ConfigProto(allow_soft_placement=True)
    sess = tf.Session(config=config)

    # One fully connected layer: (tag_size+1)*vect_size ~ 200
    inp = tf.placeholder(tf.float32, shape=(1, vect_size))
    W1 = tf.Variable(tf.random_uniform([vect_size, 2*vect_size], -0.1, 0.1))
    b1 = tf.Variable(tf.random_uniform([2*vect_size], -0.1, 0.1))
    h1 = tf.nn.relu(tf.matmul(inp, W1) + b1)
    W2 = tf.Variable(tf.random_uniform([2*vect_size, 2*vect_size], -0.1, 0.1))
    b2 = tf.Variable(tf.random_uniform([2*vect_size], -0.1, 0.1))
    h2 = tf.nn.relu(tf.matmul(h1, W2) + b2)
    W3 = tf.Variable(tf.random_uniform([2*vect_size, tag_size], -0.1, 0.1))
    b3 = tf.Variable(tf.random_uniform([tag_size], -0.1, 0.1))
    h3 = tf.nn.relu(tf.matmul(h2, W3) + b3)
    y = tf.nn.softmax(h3)

    # Error function
    labeled_y = tf.placeholder(tf.float32, shape=(1, tag_size))
    cross_entropy = tf.reduce_mean(-tf.reduce_sum(labeled_y * tf.log(y), reduction_indices=[1]))

    # Clipping the gradient
    optimizer = tf.train.AdamOptimizer(1e-5)
    gvs = optimizer.compute_gradients(cross_entropy)
    capped_gvs = [(tf.clip_by_value(grad, -1, 1), var) for grad, var in gvs]
    train_step = optimizer.apply_gradients(capped_gvs)

    sess.run(tf.initialize_all_variables())

    labels_dict = {labels[i]: i for i in range(len(labels))}

    # Populate the examples
    total_labels = {}
    for label in labels:
        total_labels[label] = 0
    training_x_and_y = []
    test_x_and_y = []
    for id in sorted(id_to_label.keys(), key=lambda x: random.random()):
        label = id_to_label[id]
        total = total_labels[label]
        if total > 30:
            continue
        total_labels[label] = total + 1
        X = np.array(id_to_centroid[id])
        y = np.array(get_vector(labels_dict[label], len(labels)))
        if total > 25:
            test_x_and_y.append([X, y, label])
        else:
            training_x_and_y.append([X, y, label])

    # Train the system
    try:
        for epoch in range(200000):
            epoch_losses = []
            training_x_and_y = sorted (training_x_and_y, key= lambda x : random.random())
            for pair_x_and_y in training_x_and_y:
                X = pair_x_and_y[0]
                y = pair_x_and_y[1]
                label = pair_x_and_y[2]
                feed_dict = {inp: [X]}
                feed_dict.update({labeled_y: [y]})
                loss, _ = sess.run([cross_entropy, train_step], feed_dict)
                print (loss)
                epoch_losses.append(loss)
            sys.stdout.flush()
            if max(epoch_losses) < 1e-1:
                break
    except:
        pass

    saver = tf.train.Saver()
    saver.save (sess, 'results/tagging_model.tf')

    file = open('results/test_labels.txt', 'w')
    for pair_x_and_y in test_x_and_y:
        X = pair_x_and_y[0]
        y = pair_x_and_y[1]
        label = pair_x_and_y[2]
        file.write(str(X)
                   + '  '
                   + str(y)
                   + '  '
                   + str(label)
                   + '\n')