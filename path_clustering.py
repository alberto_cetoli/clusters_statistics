import matplotlib.pyplot as plt
import networkx as nx
from scipy.cluster import hierarchy
from scipy.spatial import distance
from collections import defaultdict
import numpy as np
import sys
import copy
import random
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans
from gensim.models import Doc2Vec
from bhtsne import tsne

sys.setrecursionlimit(5000)

def chunks(l, n):
    n = max(1, n)
    return [l[i:i+n] for i in range(0, len(l), n)]

def girvan_newman (components, depth, num_clusters):
    components = sorted(components, key=lambda x: -len(x.nodes()))
    G = components [0]
    if depth < 0:
        return components

    def find_best_edge(G0):
        eb = nx.edge_betweenness_centrality(G0)
        eb_il = eb.items()
        eb_il = sorted(eb_il, key=lambda x: x[1], reverse=True)
        return eb_il[0][0]

    print ('Depth:', depth)
    for component in components:
        print ('len(component)',len(component.nodes()))
    print ('---')
    G.remove_edge(*find_best_edge(G))
    new_components = list(nx.connected_component_subgraphs(G))

    components = components[1:]
    components += new_components

    components = girvan_newman(components, depth - 1, num_clusters)

    return components

def load_sentiment_from(filename):
    file = open(filename)
    lines = file.readlines()[1:]

    x_dict = {}
    y_dict = {}
    all_labels= set()
    website_ids = []
    website_xy_dict = {}
    website_id_to_xy_dict = {}
    labels_list = []
    for line in lines:
        row = line.split()
        x = float(row[0])
        y = float(row[1])
        website_id = int(row[2].replace('ID_',''))
        label = row[3]
        try:
            x_dict[label].append(x)
        except:
            x_dict[label] = [x]
        try:
            y_dict[label].append(y)
        except:
            y_dict[label] = [y]
        website_ids.append(website_id)
        all_labels.add(label)
        website_xy_dict[website_id] = (x,y)
        website_id_to_xy_dict[(x, y)] = website_id
        labels_list.append(label)

    all_labels = sorted(list(all_labels))
    labels_dict = {all_labels[i]: i for i in range(len(all_labels))}

    return x_dict, y_dict, website_xy_dict, labels_dict, all_labels, website_id_to_xy_dict, website_ids, labels_list

def plot_sentiment (all_clusters):
    colors = ['r','g','b','y','m','c','k']
    for label in all_labels:

        x_axis = x_dict[label]
        y_axis = y_dict[label]
        plt.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= label)
    plt.legend(loc='best')

def plot_girvan_newman_clusters (all_clusters):
    colors = ['r','g','b','y','m','c','k']
    for label in all_labels:

        x_axis = x_dict[label]
        y_axis = y_dict[label]
        plt.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= label)
    plt.legend(loc='best')

def get_sequence_edges (website_sequence_dict, website_xy_dict, temporal_cluster_num):
    sequence_x = []
    sequence_y = []
    sequence_labels_dict = {}
    website_id_sequence = []

    index = 0
    for website_id in website_sequence_dict[temporal_cluster_num]:
        try:
            x, y = website_xy_dict[website_id]
            if x == -1 and y == -1:
                continue
            index += 1
            website_id_sequence.append(website_id)
            sequence_x.append(x)
            sequence_y.append(y)
            sequence_labels_dict [str(index)] = (x,y)
        except:
            pass
    edges = []
    for i in range(len(website_id_sequence)-1):
        curr = website_id_sequence [i]
        next = website_id_sequence [i+1]
        edges.append ([curr,next])
    return edges

def load_temporal_clusters (filename):
    file = open(filename)
    lines = file.readlines()[1:]
    website_sequence_dict = {}
    for line in lines:
        rows = line.replace(' ','').split(';')
        id = int(rows [0])
        websites_list = rows [2].replace('[','').replace(']','').replace('\n','').split(',')
        websites_list = [int(id) for id in websites_list]
        website_sequence_dict[id] = websites_list
    return website_sequence_dict

x_dict, y_dict, website_xy_dict, labels_dict, all_labels, website_id_to_xy_dict, website_ids, labels_list = load_sentiment_from ('results/svn-groups-rbf0.05-7-no-noise.txt')
website_sequence_dict = load_temporal_clusters ('results/temporal_clusters.txt')
start = 0
end = 5000
all_edges = []
for id in website_sequence_dict.keys():
    if int(id) > start and int(id) < end:
        edges = get_sequence_edges (website_sequence_dict, website_xy_dict, id)
        all_edges += edges


g = nx.Graph ()
g.add_nodes_from (website_xy_dict.keys ())
g.add_edges_from (all_edges)


xy_list = list(website_id_to_xy_dict.keys())
#sklearn_clusters = DBSCAN(eps=0.5, min_samples=2).fit(xy_list)
sklearn_clusters = KMeans(n_clusters=100).fit(xy_list)
xy_list_dict = {}
for i, label in enumerate(sklearn_clusters.labels_):
    try:
        xy_list_dict[label].append(website_id_to_xy_dict[xy_list[i]])
    except:
        xy_list_dict[label] = [website_id_to_xy_dict[xy_list[i]]]

nodes_chunks = []
for key in xy_list_dict.keys():
    nodes_chunks.append(xy_list_dict[key])

model = Doc2Vec.load('./nltk-websites-500.d2v')

new_website_xy_dict = {}
cluster_dict = {}
for i, chunk in enumerate(nodes_chunks):
    print (chunk)
    coordinates = []
    for id in chunk:
        num_docs = 0
        try:
            coordinates.append(model.docvecs['ID_'+str(id)])
            num_docs += 1
        except:
            print (id)
    com = np.sum(coordinates,0)/float(num_docs)
    new_website_xy_dict [i] = com
    cluster_dict [i] = chunk

blocks_graph = nx.blockmodel (g, nodes_chunks, multigraph = True)
g = blocks_graph

distance_matrix = np.zeros((len(g.nodes()),len(g.nodes())), dtype=np.float32)

for v1 in g.nodes():
    for v2 in g.nodes():
        number_of_edges = g.number_of_edges(v1,v2)
        if number_of_edges != 0:
            distance_matrix[v1][v2] = number_of_edges

print (distance_matrix.shape)
print ('Diagonalizing...')
w, v = np.linalg.eig(distance_matrix)
for i in range(len(w)):
    print (i,w[i])

plt.plot(w)
plt.show()

for i in range(len(v[0])):
    print (i,v[1][i])

plt.plot(v[1])
plt.show()

new_basis = []
for i in range(len(w)):
    new_vector = np.zeros(len(w))
    for j in range(len(v[i])):
        factor = v[i][j]/w[i]
        new_vector = np.add(new_vector, new_website_xy_dict[i]*factor)
    new_basis.append(new_vector)

doc_ids = website_xy_dict.keys()
new_doc_vectors = []
new_doc_ids = []
for id in website_ids:
    vector = model.docvecs['ID_'+str(id)]
    new_vector = []
    for basis_vect in new_basis:
        new_vector.append(np.dot(vector, basis_vect))
    new_doc_vectors.append(new_vector)
    new_doc_ids.append(id)

new_doc_vectors = np.array(new_doc_vectors)
projected = tsne (new_doc_vectors, dimensions=2, perplexity=50)

x = np.array(projected[:,0])
y = np.array(projected[:,1])
fig, ax = plt.subplots()

colors = []
for i in range(len(x)):
    colors.append(labels_dict[labels_list[i]])
ax.scatter(x, y, marker = '.', c=colors)
plt.show()