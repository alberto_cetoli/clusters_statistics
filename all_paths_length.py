import matplotlib.pyplot as plt
import numpy as np
import sys
from gensim.models import Doc2Vec

def load_sentiment_from(filename):
    file = open(filename)
    lines = file.readlines()[1:]

    x_dict = {}
    y_dict = {}
    all_labels= set()
    website_ids = []
    website_xy_dict = {}
    for line in lines:
        row = line.split()
        x = float(row[0])
        y = float(row[1])
        website_id = int(row[2].replace('ID_',''))
        label = row[3]
        try:
            x_dict[label].append(x)
        except:
            x_dict[label] = [x]
        try:
            y_dict[label].append(y)
        except:
            y_dict[label] = [y]
        website_ids.append(website_id)
        all_labels.add(label)
        website_xy_dict[website_id] = (x,y)

    all_labels = sorted(list(all_labels))
    labels_dict = {all_labels[i]: i for i in range(len(all_labels))}

    return x_dict, y_dict, website_xy_dict, labels_dict, all_labels

def plot_sentiment (x_dict, y_dict, labels_dict, all_labels):
    colors = ['r','g','b','y','m','c','k']
    for label in all_labels:

        x_axis = x_dict[label]
        y_axis = y_dict[label]
        plt.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= label)
    plt.legend(loc='best')

def get_lengths_of_one_sequence (website_sequence_dict, website_xy_dict, temporal_cluster_num):
    sequence_x = []
    sequence_y = []
    sequence_labels_dict = {}
    website_id_sequence = []

    index = 0
    for website_id in website_sequence_dict[temporal_cluster_num]:
        try:
            x, y = website_xy_dict[website_id]
            if x == -1 and y == -1:
                continue
            index += 1
            website_id_sequence.append(website_id)
            sequence_x.append(x)
            sequence_y.append(y)
            sequence_labels_dict [str(index)] = (x,y)
        except:
            pass
    sequence_lengths = []
    for i in range(len(sequence_x)-1):
        curr_x = sequence_x[i]
        curr_y = sequence_y[i]
        next_x = sequence_x[i+1]
        next_y = sequence_y[i+1]

        length = np.linalg.norm (np.array([curr_x,curr_y]) - np.array([next_x,next_y]))
        sequence_lengths.append (length)

    return (sequence_lengths)

def get_lengths_of_one_sequence_with_word2vec (website_sequence_dict, model, temporal_cluster_num):
    sequence = []

    index = 0
    for website_id in website_sequence_dict[temporal_cluster_num]:
        try:
            x = model.docvecs['ID_'+str(website_id)]
            sequence.append(x)
        except:
            pass
    sequence_lengths = []
    for i in range(len(sequence)-1):
        curr = sequence[i]
        next = sequence[i+1]
        length = np.linalg.norm (curr - next)
        sequence_lengths.append (length)
    return (sequence_lengths)


def load_temporal_clusters (filename):
    file = open(filename)
    lines = file.readlines()[1:]
    website_sequence_dict = {}
    for line in lines:
        rows = line.replace(' ','').split(';')
        id = int(rows [0])
        websites_list = rows [2].replace('[','').replace(']','').replace('\n','').split(',')
        websites_list = [int(id) for id in websites_list]
        website_sequence_dict[id] = websites_list
    return website_sequence_dict

def measure_all_2D_lengths (website_sequence_dict, website_xy_dict, start, end):
    all_lengths = []
    for id in website_sequence_dict.keys():
        if id > start and id < end:
            lengths = get_lengths_of_one_sequence(website_sequence_dict, website_xy_dict, id)
            all_lengths += lengths
    return all_lengths

def measure_all_word2vec_lengths (website_sequence_dict, model, start, end):
    all_lengths = []
    for id in website_sequence_dict.keys():
        if id > start and id < end:
            lengths = get_lengths_of_one_sequence_with_word2vec(website_sequence_dict, model, id)
            all_lengths += lengths
    return all_lengths

def bin_lengths (lengths, dx):
    bin_dict = {}
    for i in range(int(max(lengths)/dx)+1):
        bin_dict[i*dx] = 0
    all_lengths = []
    for length in lengths:
        approx_length = float(int(length/dx)*dx)
        bin_dict[approx_length] += 1
        all_lengths.append(approx_length)
    return all_lengths, bin_dict


def plot_lengths (bin_dict, dx):
    x_list = []
    y_list = []
    for length in bin_dict.keys():
        x_list.append(length)
        y_list.append(bin_dict[length])
    plt.bar(x_list, y_list, width = dx)

x_dict, y_dict, website_xy_dict, labels_dict, all_labels = load_sentiment_from ('results/svn-groups-rbf0.05-6-no-noise.txt')
website_sequence_dict = load_temporal_clusters ('results/temporal_clusters.txt')

start = 0
end = 5000
all_lengths = measure_all_2D_lengths (website_sequence_dict, website_xy_dict, start, end)
dx = 5
binned_lengths, bin_dict = bin_lengths (all_lengths, dx)
plot_lengths (bin_dict, dx)
plt.show()


model = Doc2Vec.load('./websites-475.d2v')
all_lengths = measure_all_word2vec_lengths (website_sequence_dict, model, start, end)
dx = 0.05
binned_lengths, bin_dict = bin_lengths (all_lengths, dx)
plot_lengths (bin_dict, dx)
plt.show()
