What is this for
===============

We need to cluster together user actions

Type
```python
python3 user_data_clustering.py
```

to get an output as in the following
```bash
Processing user ID 12.
 Average cluster length: 50.66528596944515 seconds.
 Min cluster length: 0.0 seconds.
 Max cluster length: 464.2106699999422 seconds.
 Average separation between clusters: 3281.882311730617 seconds.
 Min separation between clusters: 75.02094799932092 seconds.
 Max separation between clusters: 230647.77216199972 seconds.
```


Usage
=====

1) get_all_visited_websites.py
2) train_word2vec_model.py
3) low_dim_semantic_cluster_hierarchical.py
4) cp results/svn-groups.txt  results/svn-groups-rbf0.05-*-no-noise.txt
4) write_user_data_clustering.py
5) all_sentiment_paths.py	
6) block_modelling.c
7) cp results/block-modelling-groups.txt results/block-modelling-groups-*.txt
8) block_modelling_clusters_with_svm.py
9) block_modelling_clusters_with_svm_and_label_tags.py

Requirements
============

* psycopg2
* matplotlib
* scikit-learn