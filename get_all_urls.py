from sklearn.svm import SVC
import psycopg2
import matplotlib.pyplot as plt
import sys

def connect_and_get_cursor (dbname, user, host, password):

    connect_str = ( ' dbname='   + str(dbname)
                  + ' user='     + str(user)
                  + ' host='     + str(host)
                  + ' password=' + str(password) )
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
    return cursor

def get_all_ids_and_urls (cursor):
    cursor.execute('SELECT id, url from contextapi_website OFFSET 0 LIMIT 25000;')
    rows = cursor.fetchall()

    ids = []
    urls = []
    for row in rows:
        ids.append (row[0])
        urls.append (row [1])

    return ids, urls


cursor = connect_and_get_cursor(dbname='cscout',
                                user='andrew',
                                host='www.csut01ltvbp1.eu-west-1.rds.amazonaws.com',
                                password='efadmintestpassword')

ids, urls = get_all_ids_and_urls(cursor)

file = open ('website_data/urls.txt', 'w')
for i, _ in enumerate(ids):
    file.write (str(ids[i]) + ' ' + urls[i] + '\n')
