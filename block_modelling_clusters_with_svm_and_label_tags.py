from sklearn.svm import SVC
import matplotlib.pyplot as plt
from gensim import utils
from gensim.summarization import keywords
from gensim.models import Doc2Vec
import sys
import numpy as np
from os import listdir
from os.path import isfile, join
import nltk

def clean_document (text):
    lines = text.split('\n')
    all_lines = []
    for l in lines:
        if len(l.split()) > 4:
            all_lines.append(l)
    '\n'.join(all_lines)
    return text

def load_sentiment_from(filename):
    file = open(filename)
    lines = file.readlines()[1:]

    x_dict = {}
    y_dict = {}
    all_labels= set()
    website_ids = []
    website_xy_dict = {}
    xy = []
    labels = []
    labels_to_website_id_dict = {}
    for line in lines:
        row = line.split()
        x = float(row[0])
        y = float(row[1])
        website_id = int (row[2].replace('ID_',''))
        label = row[3]
        try:
            x_dict[label].append(x)
        except:
            x_dict[label] = [x]
        try:
            y_dict[label].append(y)
        except:
            y_dict[label] = [y]
        website_ids.append(website_id)
        all_labels.add(label)
        website_xy_dict[website_id] = (x,y)
        xy.append((x,y))
        labels.append(label)
        try:
            labels_to_website_id_dict[label].append(website_id)
        except:
            labels_to_website_id_dict[label] = [website_id]

    all_labels = sorted(list(all_labels))
    labels_dict = {all_labels[i]: i for i in range(len(all_labels))}

    return x_dict, y_dict, website_xy_dict, labels_dict, all_labels, xy, labels, website_ids, labels_to_website_id_dict

def plot_sentiment (x_dict, y_dict, labels_dict, all_labels):
    colors = ['r','g','b','y','m','c','k']
    for label in all_labels:
        x_axis = x_dict[label]
        y_axis = y_dict[label]
        plt.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= label)
    plt.legend(loc='best')

def infer_vector_from_doc (model, text):
    tokenizer = nltk.tokenize.TweetTokenizer()
    words = tokenizer.tokenize(utils.to_unicode(text))
    vector = model.infer_vector (words)
    return vector

def get_tagged_labels (model, docs, labels_to_website_id_dict):
    tagged_labels_dict = {}

    for label in labels_to_website_id_dict.keys():
        print (label)
        website_ids = labels_to_website_id_dict[label]
        doc_keywords = []
        doc_key_vectors = []
        keywords_freq_dict = {}
        for id in website_ids:
            text = docs[str(id)][0:10000]
            keyws = []
            try:
                keyws = keywords(text, split=True)
            except:
                pass
            for key in keyws:
                try:
                    keywords_freq_dict[key] += 1
                except:
                    keywords_freq_dict[key] = 1
                doc_keywords.append(key)
        doc_keywords = list(set(doc_keywords))
        doc_keywords = np.repeat(doc_keywords, 2)
        for key in doc_keywords:
            key_vector = infer_vector_from_doc(model, key)
            doc_key_vectors.append(key_vector)

        lines = []
        distances = []
        vectors = []
        for i, _ in enumerate(doc_keywords):
            key = doc_keywords[i]
            vector = doc_key_vectors[i]
            lines.append(key)
            vectors.append(vector)
            distance = 0
            #distance += 1./keywords_freq_dict[key]
            for id in website_ids:
                try:
                    doc_vector = model.docvecs['ID_'+str(id)]
                    distance += 1./(np.linalg.norm(doc_vector - vector) + 1e-5)
                except:
                    pass
            distances.append(distance)

        lvd = zip (lines, vectors, distances)
        lvd = sorted (lvd, key=lambda x: -x[2])
        most_important_line = lvd [0][0]
        tagged_labels_dict [label] = most_important_line
        keys_weight_dict = {}
        for i in range(len(lvd)):
            try:
                keys_weight_dict[lvd[i][0]] += lvd[i][2]
            except:
                keys_weight_dict[lvd[i][0]] = lvd[i][2]

        list_key_value = [ [k,v] for k, v in keys_weight_dict.items() ]
        list_key_value = sorted(list_key_value, key = lambda x : -x[1])
        for i in range(20):
            print (i, list_key_value[i])
        input_var = input("Enter the chosen label: ")
#        tagged_labels_dict[label] = list_key_value[int(input_var)][0]
        tagged_labels_dict[label] = str(input_var)


    return tagged_labels_dict

x_dict, y_dict, website_xy_dict, labels_dict, all_labels, xy, labels, website_ids, labels_to_website_id_dict = load_sentiment_from ('results/block-modelling-groups-13.txt')
plot_sentiment (x_dict, y_dict, labels_dict, all_labels)
plt.show()



path = 'website_data/'
all_files = [f for f in listdir(path) if isfile(join(path, f))]
WINDOW_SIZE = 20

docs = {}
for source in all_files:
    text_file = open(path+source, "r")
    text = text_file.read().replace('  ',' ').replace('  ',' ')
    text = clean_document (text)
    id = source.split('.')[0]
    docs[id] = text

model = Doc2Vec.load('./cleaned-nltk-websites-500.d2v')
tagged_labels_dict = get_tagged_labels (model, docs, labels_to_website_id_dict)
print (tagged_labels_dict)

_, _, _, _, _, x_and_y, _, website_ids, _ = load_sentiment_from ('results/svn-groups-rbf0.05-13-no-noise.txt')


svc = SVC(decision_function_shape='ovo', kernel='rbf', gamma = 0.2)
svc.fit(xy, labels)
sklearn_labels = svc.predict(x_and_y)

x_dict = {-1: []}
y_dict = {-1: []}
for i in labels:
    x_dict[i] = []
    y_dict[i] = []

all_labels = set()
semantic_ids = []
for i, label in enumerate(sklearn_labels):
    x_dict[label].append(x_and_y[i][0])
    y_dict[label].append(x_and_y[i][1])
    all_labels.add(label)
    semantic_ids.append (label)

file = open ('results/svn-blockmodelling-groups.txt', 'w')
file.write ('There are ' + str(len(all_labels)) + ' labels.\n')
for i, label in enumerate(sklearn_labels):
    file.write (str(x_and_y[i][0]) + ' '
                + str(x_and_y[i][1]) + ' '
                + str(website_ids[i]) + ' '
                + str(tagged_labels_dict[label]) + '\n')

colors = ['m','y','b','c','g','r','k']
for label in all_labels:
    x_axis = x_dict[label]
    y_axis = y_dict[label]
    plt.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= tagged_labels_dict[label])

plt.legend(loc='best')
plt.show()



