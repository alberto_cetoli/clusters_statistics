from gensim import utils
from gensim.models import Doc2Vec
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

model = Doc2Vec.load('./sentiment-4.d2v')
print (model.most_similar('key'))
test_vect = model.infer_vector(['The','commission','launched','an','investigation'])
print (test_vect)
print (model.docvecs['ID_1'])
print (model.docvecs.most_similar(['ID_4793']))
print (len(model.docvecs))

doc_vectors = []
doc_ids = list(model.docvecs.doctags.keys())
for id in doc_ids:
    doc_vectors.append (np.array(model.docvecs[id]))

#print (model.docvecs.doctags.values())


pca = PCA (n_components=100)
projected = pca.fit_transform (doc_vectors)
print (pca.explained_variance_ratio_)
plt.plot(pca.explained_variance_ratio_)
plt.show()

x = np.array(projected[:,0])
y = np.array(projected[:,1])
plt.scatter(x, y, marker = '.')
plt.show()


x = np.array(projected[:,0])
y = np.array(projected[:,1])
z = np.array(projected[:,2])

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.scatter(x,y,z, marker='o')
plt.show()
