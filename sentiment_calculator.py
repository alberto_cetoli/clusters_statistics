import psycopg2
import numpy as np
from gensim import utils
from gensim.models import Doc2Vec
from bs4 import BeautifulSoup


class SentimentCalculator:
    def __init__ (self, cursor, model):
        self.cursor = cursor
        self.model = model

    def __get_rows_from_ids (self, ids):
        ids_str = str(ids).strip('[]')
        self.cursor.execute('SELECT * from contextapi_website  WHERE ID IN (' + ids_str + ');')
        rows = self.cursor.fetchall()
        return rows

    def __get_docs_from_rows (self, rows):
        docs = []
        for row in rows:
            docs.append (row[3])
        return docs
    def __get_ids_from_rows (self, rows):
        docs = []
        for row in rows:
            docs.append (row[0])
        return docs

    def __get_words_from_doc (self, doc):
        soup = BeautifulSoup (doc, from_encoding='utf-8')
        for script in soup(["script", "style"]):
            script.extract()
        text = soup.get_text()
        lines = (line.strip() for line in text.splitlines())
        chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
        text = '\n'.join(chunk for chunk in chunks if chunk)
        return utils.to_unicode(text).split()

    def __infer_vectors_from_docs (self, docs):
        vectors = []
        for doc in docs:
            vectors.append (self.model.infer_vector (self.__get_words_from_doc (doc)))
        return vectors

    def __get_most_similar_in_model (self, docs):
        id_str = self.model.docvecs.most_similar ( docs ) [0][0]
        return self.model.docvecs[id_str]

    def get_closest_ids (self, unit_ids, inspected_ids, min_distance = 0.3):
        unit_rows = self.__get_rows_from_ids (unit_ids)
        unit_docs = self.__get_docs_from_rows (unit_rows)
        inspected_rows = self.__get_rows_from_ids(inspected_ids)
        inspected_docs = self.__get_docs_from_rows (inspected_rows)
        inspected_ids  = self.__get_ids_from_rows (inspected_rows)
        unit_vectors = self.__infer_vectors_from_docs (unit_docs)
        closest_unit = self.__get_most_similar_in_model ( unit_vectors )
        inspected_vectors = self.__infer_vectors_from_docs (inspected_docs)

        closest_ids = []
        for index, vector in enumerate(inspected_vectors):
            closest_vector = self.__get_most_similar_in_model ([vector])
            distance = 1. / np.linalg.norm(closest_unit - closest_vector)
            if distance > min_distance:
                closest_ids.append (inspected_ids [index])

        return closest_ids


def connect_and_get_cursor (dbname, user, host, password):

    connect_str = ( ' dbname='   + str(dbname)
                  + ' user='     + str(user)
                  + ' host='     + str(host)
                  + ' password=' + str(password) )
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
    return cursor


cursor = connect_and_get_cursor (dbname = 'cscout',
                                 user   = 'andrew',
                                 host   = '46.101.78.187',
                                 password = 'efadmintestpassword')


model = Doc2Vec.load('./websites-475.d2v')

calculator = SentimentCalculator (cursor, model)
closest_ids = calculator.get_closest_ids ([4793], [7441, 123, 124, 1])

print (closest_ids)

print (model.docvecs.similarity('ID_4793', 'ID_7441'))
print (model.docvecs.similarity('ID_4793', 'ID_123'))

#print (model.docvecs['ID_4793'])
print (model.docvecs.similarity('ID_4793','ID_7441'))
print (model.docvecs.similarity('ID_4793','ID_123'))
print (model.docvecs.similarity('ID_4793','ID_124'))

print ('')
print (np.dot (model.docvecs['ID_4793'], model.docvecs['ID_7441']) / 100.)
print (np.dot (model.docvecs['ID_4793'], model.docvecs['ID_123']) / 100.)
print (np.dot (model.docvecs['ID_4793'], model.docvecs['ID_124']) / 100.)

print ('')
print (1./np.linalg.norm(model.docvecs['ID_4793'] - model.docvecs['ID_7441']))
print (1./np.linalg.norm(model.docvecs['ID_4793'] - model.docvecs['ID_123']))
print (1./np.linalg.norm(model.docvecs['ID_4793'] - model.docvecs['ID_124']))
