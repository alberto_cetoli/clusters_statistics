from gensim import utils
from gensim.models import Doc2Vec
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def find_new_coord_using_centroids (vector, centroids):
    coords = []
    for centroid in centroids:
        coords.append(np.linalg.norm(vector - centroid))
    return coords

model = Doc2Vec.load('./websites-475.d2v')


cluster_file = open('./results/tsne-groups-perplexity25_quant0.02.txt')
lines = cluster_file.readlines()[1:]
cluster_dict = {-1:[]}
for i in range(1000):
    cluster_dict[i] = []
for line in lines:
    words = line.split()
    id = str(words [2])
    cluster_num = int(words[3])
    cluster_dict[cluster_num].append(id)

clusters = []
for key in cluster_dict.keys():
    cluster = cluster_dict[key]
    clusters.append(cluster)

clusters = sorted(clusters, key = lambda x: -len(x))

MAX_DIM = 10

clusters = clusters[:MAX_DIM]

# compute the centroids
centroids = []
for cluster in clusters:
    vectors = []
    for id in cluster:
        vectors.append(model.docvecs[id])
    centroid = np.sum(vectors, 0)/len(vectors)
    centroids.append(centroid)

all_vectors = []
all_ids = []

for cluster in clusters:
    vectors = []
    ids = []

    for id in cluster:
        vectors.append(model.docvecs[id])
        ids.append(id)

    for i, vector in enumerate(vectors):
        new_coords = find_new_coord_using_centroids (vector, centroids)
        all_vectors.append(new_coords)
        all_ids.append(ids[i])

file = open ('results/centroid-coordinates-docs.txt', 'w')
for i in range(len(all_vectors)):
    file.write (str(all_ids[i])
                + ' | '
                + str(all_vectors[i])
                + '\n')