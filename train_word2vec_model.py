from os import listdir
from os.path import isfile, join
from gensim import utils
from gensim.models.doc2vec import TaggedDocument
from gensim.models import Doc2Vec
import numpy
from random import shuffle
import copy
import nltk


from os import listdir
from os.path import isfile, join
path = 'website_data/'
all_files = [f for f in listdir(path) if isfile(join(path, f))]

WINDOW_SIZE = 20

files = []
ids = []
for source in all_files:
    text_file = open(path+source, "r")
    text = text_file.read()
    words = utils.to_unicode(text).split()
    if len(words) < WINDOW_SIZE:
        continue
    files.append(source)
    ids.append(source.split('.')[0])


class LabeledLineSentence(object):
    def __init__(self, sources):
        self.sources = sources

        flipped = {}

        # make sure that keys are unique
        for key, value in sources.items():
            if value not in flipped:
                flipped[value] = [key]
            else:
                raise Exception('Non-unique prefix encountered')

    def to_array(self):
        self.sentences = []
        for source, prefix in self.sources.items():
            text_file = open(source, "r")
            text = text_file.read()
            tokenizer = nltk.tokenize.TweetTokenizer()
            words = tokenizer.tokenize(utils.to_unicode(text))
#            words = utils.to_unicode(text).split()

            self.sentences.append(TaggedDocument(words=words, tags=[prefix]))
        return self.sentences

    def sentences_perm(self):
        shuffle (self.sentences)
        return self.sentences


sources = {}

for i in range(len(files)):
    file = files[i]
    id = ids[i]
    sources[path+file] = 'ID_' + str(id)

sentences = LabeledLineSentence(sources)

model = Doc2Vec(min_count=WINDOW_SIZE+1, window=WINDOW_SIZE, size=100, sample=1e-4, negative=5, workers=8)
model.build_vocab(sentences.to_array())

#model = Doc2Vec.load('nltk-websites-400.d2v')
#sentences.to_array()

for epoch in range(0, 501):
    print ('Training epoch ' + str(epoch) + '.')
    model.train(sentences.sentences_perm())
    if epoch % 50 == 0:
        model.save('./cleaned-nltk-websites-'+str(epoch)+'.d2v')
