from gensim import utils
from gensim.models import Doc2Vec
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

model = Doc2Vec.load('./no_punct_websites-225.d2v')
print (model.most_similar('key'))
test_vect = model.infer_vector(['The','commission','launched','an','investigation'])
print (test_vect)
print (model.docvecs['ID_1'])
print (model.docvecs.most_similar(['ID_4793']))
print (len(model.docvecs))

doc_vectors = []
doc_ids = list(model.docvecs.doctags.keys())
for id in doc_ids:
    doc_vectors.append (np.array(model.docvecs[id]))

#print (model.docvecs.doctags.values())


pca = PCA (n_components=100)
projected = pca.fit_transform (doc_vectors)
print (pca.explained_variance_ratio_)
plt.plot(pca.explained_variance_ratio_)
plt.show()

x = np.array(projected[:,0])
y = np.array(projected[:,1])
plt.scatter(x, y, marker = '.')
plt.show()


x = np.array(projected[:,0])
y = np.array(projected[:,1])
z = np.array(projected[:,2])

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.scatter(x,y,z, marker='o')
plt.show()

print ('Components:')
component_x = pca.components_[:,0]
component_y = pca.components_[:,1]
component_z = pca.components_[:,2]
print (model.most_similar([component_x]))
print (model.most_similar([component_y]))
print (model.most_similar([component_z]))

x_and_y = []
for index, _ in enumerate(x):
    x_and_y.append([y[index], x[index]])
#    x_and_y.append([y[index], x[index], z[index]])

NUM_CLUSTERS = 4000

x_dict = {-1:[]}
y_dict = {-1:[]}
for i in range(NUM_CLUSTERS):
    x_dict[i] = []
    y_dict[i] = []

#sklearn_clusters = KMeans(n_clusters=NUM_CLUSTERS).fit(x_and_y)
sklearn_clusters = DBSCAN(eps=0.25, min_samples=5).fit(x_and_y) # 2D
#sklearn_clusters = DBSCAN(eps=0.2, min_samples=5).fit(x_and_y) # 3D

all_labels = set()
for i, label in enumerate(sklearn_clusters.labels_):
    x_dict[label].append(x[i])
    y_dict[label].append(y[i])
    all_labels.add(label)

colors = ['r','g','b','y','m','c','k']
for label in all_labels:
    x_axis = x_dict[label]
    y_axis = y_dict[label]
    plt.scatter(x_axis, y_axis, marker = '.', color=colors[label%len(colors)])
plt.show()

file = open ('groups.txt', 'w')
for i, label in enumerate(sklearn_clusters.labels_):
    file.write (str([i, x[i], y[i], doc_ids[i], colors[label%len(colors)]])+'\n')


x_axis = x_dict[0]
y_axis = y_dict[0]
plt.scatter(x_axis, y_axis, marker = '.', color=colors[label%len(colors)])
plt.show()

doc_vectors = []
for i, label in enumerate(sklearn_clusters.labels_):
    if int(label) == 0:
        id = doc_ids[i]
        doc_vectors.append (np.array(model.docvecs[id]))

pca = PCA (n_components=100)
projected = pca.fit_transform (doc_vectors)
x = np.array(projected[:,0])
y = np.array(projected[:,1])
plt.scatter(x, y, marker = '.')
plt.show()
print (pca.explained_variance_ratio_)
plt.plot(pca.explained_variance_ratio_)
plt.show()



x = np.array(projected[:,0])
y = np.array(projected[:,1])
z = np.array(projected[:,2])
x_and_y = []
for index, _ in enumerate(x):
#    x_and_y.append([y[index], x[index]])
    x_and_y.append([y[index], x[index], z[index]])

NUM_CLUSTERS = 4

x_dict = {-1:[]}
y_dict = {-1:[]}
for i in range(NUM_CLUSTERS):
    x_dict[i] = []
    y_dict[i] = []

sklearn_clusters = []
sklearn_clusters = KMeans(n_clusters=NUM_CLUSTERS).fit(x_and_y)
#sklearn_clusters = DBSCAN(eps=0.35, min_samples=5).fit(x_and_y)
all_labels = set()
for i, label in enumerate(sklearn_clusters.labels_):
    x_dict[label].append(x[i])
    y_dict[label].append(y[i])
    all_labels.add(label)

colors = ['r','g','b','y','m','c','k']
for label in all_labels:
    x_axis = x_dict[label]
    y_axis = y_dict[label]
    plt.scatter(x_axis, y_axis, marker = '.', color=colors[label%len(colors)])
plt.show()

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.scatter(x,y,z, marker='o')
plt.show()


file = open ('groups-zoom.txt', 'w')
for i, label in enumerate(sklearn_clusters.labels_):
    file.write (str([i, x[i], y[i], doc_ids[i], colors[label%len(colors)]])+'\n')


print (model.docvecs.most_similar(['ID_12696']))
print (model.docvecs.most_similar(['ID_10672']))


