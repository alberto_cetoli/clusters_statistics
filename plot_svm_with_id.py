import psycopg2
import matplotlib.pyplot as plt

def connect_and_get_cursor (dbname, user, host, password):

    connect_str = ( ' dbname='   + str(dbname)
                  + ' user='     + str(user)
                  + ' host='     + str(host)
                  + ' password=' + str(password) )
    conn = psycopg2.connect(connect_str)
    cursor = conn.cursor()
    return cursor

def load_sentiment_from(filename):
    file = open(filename)
    lines = file.readlines()[1:]

    x_dict = {}
    y_dict = {}
    all_labels= set()
    website_ids = []
    website_xy_dict = {}
    websites_ids_from_xy = {}
    xy = []
    labels = []
    for line in lines:
        row = line.split()
        x = float(row[0])
        y = float(row[1])
        website_id = int(row[2].replace('ID_',''))
        label = row[3]
        try:
            x_dict[label].append(x)
        except:
            x_dict[label] = [x]
        try:
            y_dict[label].append(y)
        except:
            y_dict[label] = [y]
        website_ids.append(website_id)
        all_labels.add(label)
        website_xy_dict[website_id] = (x,y)
        websites_ids_from_xy[(x,y)] = website_id
        xy.append((x,y))
        labels.append(label)

    all_labels = sorted(list(all_labels))
    labels_dict = {all_labels[i]: i for i in range(len(all_labels))}

    return x_dict, y_dict, website_xy_dict, labels_dict, all_labels, xy, labels, websites_ids_from_xy

def plot_sentiment_with (x_dict, y_dict, labels_dict, all_labels, websites_ids_from_xy):
    colors = ['r','g','b','y','m','c','k']
    fig, ax = plt.subplots()
    for label in all_labels:
        x_axis = x_dict[label]
        y_axis = y_dict[label]
        ax.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= label)
        for i in range(len(x_axis)):
            xy = (x_axis[i], y_axis[i])
            ax.annotate(websites_ids_from_xy[xy], xy = xy, alpha= 0.1)
    ax.legend(loc='best')


x_dict, y_dict, website_xy_dict, labels_dict, all_labels, xy, labels, websites_ids_from_xy = load_sentiment_from ('results/svn-groups-rbf0.05-7-no-noise.txt')
plot_sentiment_with(x_dict, y_dict, labels_dict, all_labels, websites_ids_from_xy)
plt.show()

