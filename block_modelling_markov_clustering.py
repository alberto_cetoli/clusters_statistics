import matplotlib.pyplot as plt
import networkx as nx
from scipy.cluster import hierarchy
from scipy.spatial import distance
from collections import defaultdict
import numpy
import sys
import copy
import random
from sklearn.cluster import DBSCAN, KMeans
from gensim.models import Doc2Vec
from os import listdir
from os.path import isfile, join
import numpy as np
import nltk
from gensim import utils
import random
from python_mcl.mcl.mcl_clustering import networkx_mcl

sys.setrecursionlimit(5000)

MAX_DISTANCE = 6
MAX_TSNE_DISTANCE = 100

def infer_vector_from_doc (model, text):
    tokenizer = nltk.tokenize.TweetTokenizer()
    words = tokenizer.tokenize(utils.to_unicode(text))
    vector = model.infer_vector (words)
    return vector


def find_closest_vector(model,text):
    vector = infer_vector_from_doc(model, text)
    most_similar_list = model.docvecs.most_similar([vector])
    most_similar_id = most_similar_list[0][0]
    return model.docvecs[most_similar_id]


def chunks(l, n):
    n = max(1, n)
    return [l[i:i+n] for i in range(0, len(l), n)]

def load_sentiment_from(filename):
    file = open(filename)
    lines = file.readlines()[1:]

    x_dict = {}
    y_dict = {}
    all_labels= set()
    website_ids = []
    website_xy_dict = {}
    website_id_to_xy_dict = {}
    for line in lines:
        row = line.split()
        x = float(row[0])
        y = float(row[1])
        website_id = int(row[2].replace('ID_',''))
        label = row[3]
        try:
            x_dict[label].append(x)
        except:
            x_dict[label] = [x]
        try:
            y_dict[label].append(y)
        except:
            y_dict[label] = [y]
        website_ids.append(website_id)
        all_labels.add(label)
        website_xy_dict[website_id] = (x,y)
        website_id_to_xy_dict[(x, y)] = website_id

    all_labels = sorted(list(all_labels))
    labels_dict = {all_labels[i]: i for i in range(len(all_labels))}

    return x_dict, y_dict, website_xy_dict, labels_dict, all_labels, website_id_to_xy_dict

def plot_sentiment (all_clusters):
    colors = ['r','g','b','y','m','c','k']
    for label in all_labels:

        x_axis = x_dict[label]
        y_axis = y_dict[label]
        plt.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= label)
    plt.legend(loc='best')

def plot_girvan_newman_clusters (all_clusters):
    colors = ['r','g','b','y','m','c','k']
    for label in all_labels:

        x_axis = x_dict[label]
        y_axis = y_dict[label]
        plt.scatter(x_axis, y_axis, marker = '.', color=colors[labels_dict[label]%len(colors)], label= label)
    plt.legend(loc='best')

def get_sequence_edges (sequence, website_xy_dict):
    sequence_x = []
    sequence_y = []
    sequence_labels_dict = {}
    website_id_sequence = []

    index = 0
    for website_id in sequence:
        try:
            x, y = website_xy_dict[website_id]
            if x == -1 and y == -1:
                continue
            index += 1
            website_id_sequence.append(website_id)
            sequence_x.append(x)
            sequence_y.append(y)
            sequence_labels_dict [str(index)] = (x,y)
        except:
            pass
    edges = []
    for i in range(len(website_id_sequence)-1):
        curr = website_id_sequence [i]
        next = website_id_sequence [i+1]
        edges.append ([curr,next])
    return edges

def load_temporal_clusters (filename):
    file = open(filename)
    lines = file.readlines()[1:]
    website_sequence_dict = {}
    for line in lines:
        rows = line.replace(' ','').split(';')
        id = int(rows [0])
        websites_list = rows [2].replace('[','').replace(']','').replace('\n','').split(',')
        websites_list = [int(id) for id in websites_list]
        website_sequence_dict[id] = websites_list
    return website_sequence_dict

def load_docs (path):
    all_files = [f for f in listdir(path) if isfile(join(path, f))]
    WINDOW_SIZE = 20

    docs = {}
    for source in all_files:
        text_file = open(path + source, "r")
        text = text_file.read().replace('  ', ' ').replace('  ', ' ')
        id = source.split('.')[0]
        docs[id] = text
    return docs


def break_into_sentiment_sequences (model, docs, sequence, website_xy_dict):
    return_sequences = []
    last_pos = 0
    for i in range(len(sequence)-1):
        try:
#            curr_text = docs[str(sequence[i])]
#            next_text = docs[str(sequence[i+1])]
            #curr = find_closest_vector(model,curr_text)
            #next = find_closest_vector(model,next_text)
            curr = model.docvecs["ID_"+str(sequence[i])]
            next = model.docvecs["ID_"+str(sequence[i+1])]
            distance = np.linalg.norm(curr-next)
            if distance > MAX_DISTANCE:
                return_sequences.append(sequence[last_pos:i])
                last_pos = i
                continue
            curr_2d = website_xy_dict[int(sequence[i])]
            next_2d = website_xy_dict[int(sequence[i+1])]
            distance_2d = np.linalg.norm(np.array(curr_2d) - np.array(next_2d))
            if distance_2d > MAX_TSNE_DISTANCE:
                return_sequences.append(sequence[last_pos:i+1])
                last_pos = i+1
                continue
        except:
            pass
    return_sequences.append(sequence[last_pos:len(sequence)])
    return return_sequences

x_dict, y_dict, website_xy_dict, labels_dict, all_labels, website_id_to_xy_dict = load_sentiment_from ('results/svn-groups-rbf0.05-13-no-noise.txt')
model = Doc2Vec.load('cleaned-nltk-websites-500.d2v')
docs = load_docs('website_data/')


website_sequence_dict = load_temporal_clusters ('results/temporal_clusters.txt')
all_edges = []
sequences_length = len(website_sequence_dict.keys())
all_ids = []
all_keys = website_sequence_dict.keys()
print ('all_keys:', len(all_keys))

NUM_CLUSTERS = 3000
TOTAL_NODES = len(docs)
RATIO = len(all_keys) #1000
NUM_STEPS = 1000 #int(1./RATIO)
print(NUM_STEPS)

all_keys =sorted(all_keys, key=lambda x: random.random())
for i, key in enumerate(all_keys[0:RATIO]):
    sequence = break_into_sentiment_sequences(model,docs,website_sequence_dict[key], website_xy_dict)
#    print (random.randint(0,TOTAL_NODES-1))
#    if random.randint(0,TOTAL_NODES-1) < RATIO:
#        continue
    all_ids.append(key)
    for item in sequence:
        edges = get_sequence_edges (item, website_xy_dict)
        all_edges += edges

print ('all_ids:', len(all_ids))
g = nx.Graph ()
g.add_nodes_from (website_xy_dict.keys ())
g.add_edges_from (all_edges)

#nodes_chunks = chunks (copy.deepcopy(sorted(list(website_xy_dict.keys ()),key= lambda x: random.random())), 100)

# Find the chunks from DBSCAN
xy_list = list(website_id_to_xy_dict.keys())
sklearn_clusters = KMeans(n_clusters=NUM_CLUSTERS, n_jobs=8).fit(xy_list)
#sklearn_clusters = DBSCAN(eps=0.5, min_samples=2).fit(xy_list)
xy_list_dict = {}
for i, label in enumerate(sklearn_clusters.labels_):
    try:
        xy_list_dict[label].append(website_id_to_xy_dict[xy_list[i]])
    except:
        xy_list_dict[label] = [website_id_to_xy_dict[xy_list[i]]]

nodes_chunks = []
for key in xy_list_dict.keys():
    nodes_chunks.append(xy_list_dict[key])

new_website_xy_dict = {}
cluster_dict = {}
for i, chunk in enumerate(nodes_chunks):
    coordinates = [website_xy_dict [id] for id in chunk]
    com = numpy.sum(coordinates,0)/len(coordinates)
    new_website_xy_dict [i] = com
    cluster_dict [i] = chunk

blocks_graph = nx.blockmodel (g, nodes_chunks, multigraph = True)
g = blocks_graph

nx.draw (g,new_website_xy_dict, node_shape = '.', alpha=0.5)
plt.show ()

M, clusters = networkx_mcl(g, expand_factor = 15, inflate_factor = 2, max_loop = 50 , mult_factor = 1)
print (clusters)

colors = ['r', 'g', 'b', 'y', 'm', 'c', 'k']
i = 0
for key in clusters.keys():
    cluster = clusters[key]
    if len(cluster) < 10:
        continue
    gg = nx.Graph()
    gg.add_nodes_from (cluster)
    nx.draw(gg, new_website_xy_dict, node_shape = '.', node_color=colors[i%len(colors)])
    i += 1
plt.show()

biggest_clusters = []
i = 0
for key in clusters.keys():
    cluster = clusters[key]
    if len(cluster) < 10:
        continue
    try:
        biggest_clusters.find(cluster)
        continue
    except:
        pass
    print (len(cluster))
    new_cluster = []
    for item in cluster:
        new_cluster += cluster_dict[item]
    biggest_clusters.append(new_cluster)
    gg = nx.Graph()
    gg.add_nodes_from (new_cluster)
    nx.draw(gg, website_xy_dict, node_shape = '.', linewidths = 0, node_size=10, node_color=colors[i%len(colors)])
    i += 1
plt.show()

file = open ('results/block-modelling-groups.txt', 'w')
file.write ('# There are ' + str(len(biggest_clusters)) + ' labels.\n')
matching_labels = 0
tot_labels = 0

for i, cluster in enumerate(biggest_clusters):
    chunk_index = i
    new_cluster = []
    for item in cluster:
        new_cluster.append(item)
    for cluster_item in new_cluster:
        x,y = website_xy_dict[cluster_item]
        file.write (str(x) + ' '
                    + str(y) + ' '
                    + str(cluster_item) + ' '
                    + str(chunk_index) + '\n')
file.close ()
